//
//  AppDelegate.swift
//  Gift3R
//
//  Created by Ranjit on 14/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import UserNotificationsUI


import IQKeyboardManagerSwift

import Instabug


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        
        if let device_Id = UIDevice.current.identifierForVendor?.uuidString {
            UserDefaults.standard.setValue(device_Id, forKey: "deviceId")
        }
        
        IQKeyboardManager.shared.enable = true
        
        
        Instabug.start(withToken: "5d41cdf60248c017b27415ee33214186", invocationEvents: [.shake, .screenshot])

        
        
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
                if !accepted {
                    print("Notification access denied.")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                
            }
            
        } else {
            // Fallback on earlier versions
        }
        
        
        if #available(iOS 10.0, *) // enable new way for notifications on iOS 10
        {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .alert , .sound]) { (accepted, error) in
                if !accepted
                {
                    print("Notification access denied.")
                }
                else
                {
                    print("Notification access accepted.")
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications();
                    })
                    
                }
            }
        }
        else
        {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound];
            let setting = UIUserNotificationSettings(types: type, categories: nil);
            UIApplication.shared.registerUserNotificationSettings(setting);
            UIApplication.shared.registerForRemoteNotifications();
        }
        //-------------------------------------------------------------------------//
        
        application.registerForRemoteNotifications()
        
        FirebaseApp.configure()
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
 
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}


extension AppDelegate {
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        
        if let refreshedToken = InstanceID.instanceID().token() {
            
            //  userData.set(InstanceID.instanceID().token() as Any, forKey: "tokenn")
            
            print("InstanceID token: \(refreshedToken)")
            
            UserDefaults.standard.set(InstanceID.instanceID().token(), forKey: "FCM_Token")
            
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        debugPrint("didRegisterForRemoteNotificationsWithDeviceToken: DATA")
        let token = String(format: "%@", deviceToken as CVarArg)
        debugPrint("*** deviceToken: \(token)")
        
        UserDefaults.standard.set(token, forKey: "deviceToken")
        
        Messaging.messaging().apnsToken = deviceToken
        debugPrint("Firebase Token:",InstanceID.instanceID().token() as Any)
        
        if(!(InstanceID.instanceID().token() == nil)) {
            UserDefaults.standard.set(InstanceID.instanceID().token(), forKey: "FCM_Token")
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        var userInfo = notification.request.content.userInfo
        //        if (userInfo[kGCMMessageIDKey] != nil) {
        //            print("Message ID: \(String(describing: userInfo[kGCMMessageIDKey]))")
        //        }
        //        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        // Print full message.
        print("\(userInfo)")
        // Change this to your preferred presentation option
        completionHandler([.sound, .alert, .badge])
        print("User Info : \(notification.request.content.userInfo)")
        
        let NotificationManage = UserDefaults.standard.object(forKey: "NotificationManage") as? String
        
        //        if NotificationManage == "1"{
        //
        //        }else{
        
        var unreadNotificationCount = UserDefaults.standard.object(forKey: "unreadNotificationCount") as? Int
        
        if unreadNotificationCount == nil {
            
            UserDefaults.standard.set(1, forKey: "unreadNotificationCount")
            
        }else {
            
            unreadNotificationCount = unreadNotificationCount! + 1
            
            UserDefaults.standard.set(unreadNotificationCount, forKey: "unreadNotificationCount")
            
        }
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        guard let json = userInfo[AnyHashable("gcm.notification.msg_data")] as? String else { return }
        
        let data = json.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
           // payloadInfo(json: json)
            
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
        
    }
    
    func payloadInfo(json: [String: AnyObject]) {
        if let info = json["data"] as? [String: AnyObject] {
            let advertisementId = info["advertisement_id"] as? Double
            let userId = info["user_id"] as? Double
            let messageType = json["type"] as? String ?? ""
        }
        
        let messageCount = json["unread_notification"] as! Int
        UserDefaults.standard.set(String(messageCount), forKey: "Msg_Count")
    }
    
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        
        debugPrint("--->messaging:\(messaging)")
        debugPrint("--->didReceive Remote Message:\(remoteMessage.appData)")
        guard let data =
            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
            let prettyPrinted = String(data: data, encoding: .utf8) else { return }
        
        print("Received direct channel message:\n\(prettyPrinted)")
        
        //        var unreadNotificationCount = UserDefaults.standard.object(forKey: "unreadNotificationCount") as? Int
        //
        //        unreadNotificationCount = unreadNotificationCount! + 1
        //
        //        UserDefaults.standard.set(unreadNotificationCount, forKey: "unreadNotificationCount")
        
    }
    
}
