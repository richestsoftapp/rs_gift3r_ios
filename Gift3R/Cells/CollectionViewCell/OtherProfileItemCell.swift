//
//  OtherProfileItemCell.swift
//  Gift3R
//
//  Created by Ranjit on 21/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

class OtherProfileItemCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameField: UILabel!
    
    fileprivate let imageLoader = ImageLoader.sharedInstance
    fileprivate var index : Int?
    
    
    func setUpCell(info: Stores) {
        nameField.text = info.name
        loadImage(defaultImage : #imageLiteral(resourceName: "LoadingImage"), url : info.image, imageView: img)
    }
    
    fileprivate func loadImage(defaultImage : UIImage?, url : String?, imageView : UIImageView?){
        imageView?.image = defaultImage
        imageLoader.loadImage(url , token: { () -> (Int) in
            return (self.index ?? 0)
        }) { (success, image) in
            if(!success){
                return
            }
            imageView?.image = image
        }
    }
    
}
