//
//  RegisteryCell.swift
//  Gift3R
//
//  Created by Ranjit on 20/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

class RegisteryCell: UICollectionViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameField: UILabel!
    
    fileprivate let imageLoader = ImageLoader.sharedInstance
    fileprivate var index : Int?
    
    
    func setUpCell(info: Favouritelist) {
        nameField.text = info.sName
        loadImage(defaultImage : #imageLiteral(resourceName: "LoadingImage"), url : info.image, imageView: img)
    }
    
    fileprivate func loadImage(defaultImage : UIImage?, url : String?, imageView : UIImageView?){
        imageView?.image = defaultImage
        imageLoader.loadImage(url , token: { () -> (Int) in
            return (self.index ?? 0)
        }) { (success, image) in
            if(!success){
                return
            }
            imageView?.image = image
        }
    }
    
}
