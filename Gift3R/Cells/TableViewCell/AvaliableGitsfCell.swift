//
//  AvaliableGitsfCell.swift
//  Gift3R
//
//  Created by Ranjit on 24/10/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

protocol AvaliableGitsProtocal {
    func info(cell: AvaliableGitsfCell)
    func buy(cell: AvaliableGitsfCell)
    func send(cell: AvaliableGitsfCell)
    func messageBtn(cell: AvaliableGitsfCell)
}

class AvaliableGitsfCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    var delegate: AvaliableGitsProtocal?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpInfo(info: StoresGift) {
        name.text = info.giftCardName
        if let priceValue = info.price {
            price.text = "$\(priceValue).00 Electronic Gift Card"
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    @IBAction func buyAction(_ sender: DesignableButton) {
        delegate?.buy(cell: self)
    }
    
    @IBAction func noteAction(_ sender: DesignableButton) {
        delegate?.messageBtn(cell: self)
    }
    
    @IBAction func sendAction(_ sender: DesignableButton) {
        delegate?.send(cell: self)
    }
    
    @IBAction func infoBtnActiopn(_ sender: UIButton) {
         delegate?.info(cell: self)
    }
    
}
