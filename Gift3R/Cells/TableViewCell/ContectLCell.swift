
//
//  ContectLCell.swift
//  Gift3R
//
//  Created by Ranjit on 18/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

class ContectLCell: UITableViewCell {

    @IBOutlet weak var inviteBtn: UIButton!
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var number: UILabel!
    
    @IBOutlet weak var profileImage: DesignableImage!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImage.layer.cornerRadius = profileImage.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
