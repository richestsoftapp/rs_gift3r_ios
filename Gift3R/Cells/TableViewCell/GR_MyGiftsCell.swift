//
//  GR_MyGiftsCell.swift
//  Gift3R
//
//  Created by Ranjit on 15/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit


protocol GR_MyGiftsProtocal {
    func info(cell: GR_MyGiftsCell)
    func redeem(cell: GR_MyGiftsCell)
    func send(cell: GR_MyGiftsCell)
    func messageBtn(cell: GR_MyGiftsCell)
    
}


class GR_MyGiftsCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var prductName: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    @IBOutlet weak var messageIcon: UIImageView!
    
    
    fileprivate let imageLoader = ImageLoader.sharedInstance
    fileprivate var index : Int?

    var delegate: GR_MyGiftsProtocal?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpInfo(info: Gifts) {
        
        if info.message == "" {
            messageIcon.image = UIImage.init(named: "messageicon")
        } else {
            messageIcon.image = UIImage.init(named: "messageicon_red")
        }
        
        userName.text = info.storeName
        prductName.text = info.giftCardName
        
        if let priceVal = info.availableBalance {
            price.text = "Available balance: $\(priceVal) Electronic Gift Card"
        }
        
        loadImage(defaultImage : #imageLiteral(resourceName: "LoadingImage"), url : info.storeImage, imageView: img)
        
    }
    
    fileprivate func loadImage(defaultImage : UIImage?, url : String?, imageView : UIImageView?){
        imageView?.image = defaultImage
        imageLoader.loadImage(url , token: { () -> (Int) in
            return (self.index ?? 0)
        }) { (success, image) in
            if(!success){
                return
            }
            imageView?.image = image
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func infoBtnAction(_ sender: UIButton) {
        delegate?.info(cell: self)
    }
    
    @IBAction func chatAction(_ sender: Any) {
        delegate?.messageBtn(cell: self)
    }
    
    @IBAction func redeemAction(_ sender: UIButton) {
        delegate?.redeem(cell: self)
    }
    
    
    @IBAction func sendAction(_ sender: UIButton) {
        delegate?.send(cell: self)
    }
    
    

}
