//
//  Urls.swift
//  Gift3R
//
//  Created by Ranjit on 17/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import Foundation

// var BASE_URL = "http://demo.richestsoft.in/GIFT3R/public/api/v1/"
var BASE_URL = "http://18.221.106.68/gift3r/public/api/v1/"

var Signup = BASE_URL + "signup"
var Login = BASE_URL + "login"
var SendOTP = BASE_URL + "otp"
var UserForgotPassword = BASE_URL + "forgot-password"

var Logout = BASE_URL + "logout"

var StoreListing = BASE_URL + "store-list"

var ChangePassword = BASE_URL + "change-password"
var SingleStore = BASE_URL + "store"

var MyCards = BASE_URL + "my-cards"

var SendMyCards = BASE_URL + "send-cards"
var RedeemCard = BASE_URL + "redeem-cards"
var BuyCard = BASE_URL + "buy-cards"


var TermsConditons = BASE_URL + "terms"
var PolicyPrivacy = BASE_URL + "policy"
var AboutUS = BASE_URL + "about-us"
var TermsOfUse = BASE_URL + "terms-use"


var addTofav = BASE_URL + "favourite-store"

var MyRegistry = BASE_URL + "myfavourite-storelist"

var payment = BASE_URL + "payment"

var inviteByUrl = BASE_URL  + "sendappurl"
var getAllContactList = BASE_URL  + "contact-list"
var getAllOtherUsersItem = BASE_URL + "favourite-storelist"


/*
 
 public class WebConstants {
 
 // public static final String ACTION_BASE_URL = "http://demo.richestsoft.com/GIFT3R/GIFT3R/public/api/v1/";
 
 //http://demo.richestsoft.in/GIFT3R/public/api/v1
 // public static final String ACTION_BASE_URL = "http://demo.richestsoft.in/GIFT3R/public/api/v1/";
 public static final String ACTION_BASE_URL = "http://18.221.106.68/gift3r/public/api/v1/";
 public static final String ACTION_ABOUT_US = ACTION_BASE_URL + "about-us";
 public static final String ACTION_TERMS_AND_CONDITIONS = ACTION_BASE_URL + "terms";
 public static final String ACTION_TERMS_OF_USE = ACTION_BASE_URL + "terms-use";
 public static final String ACTION_PRIVACY_POLICY = ACTION_BASE_URL + "policy";
 
 // Date Time Format Constants
 public static final String DATE_FORMAT_SERVER = "yyyy-MM-d";
 public static final String DATE_FORMAT_DISPLAY = "d MMM, yyyy";
 
*/


