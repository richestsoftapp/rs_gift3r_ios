//
//  GR_ForgetPasswordController.swift
//  Gift3R
//
//  Created by Ranjit on 14/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

protocol ForgotPasswordProtocol {
    func removeForgotPasswordPopUp() -> Void
}

class GR_ForgetPasswordController: UIViewController {

    @IBOutlet weak var emailField: UITextField?
    
    var forgotPasswordProtocolDelegate : ForgotPasswordProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        emailField?.text = "indeedseo@gmail.com"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupViewResizerOnKeyboardShown()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        forgotPasswordProtocolDelegate?.removeForgotPasswordPopUp()
    }

    @IBAction func forGotAction(_ sender: UIButton) {
        
        guard let email = emailField?.text, email != "" else {
            GlobalMethod.init().showAlert(title: "Email Adderss", message: "Please enter your email address", vc: self)
            return
        }
        
        if email.isValidEmail {
            forgotPassword(email: email)
        } else {
            GlobalMethod.init().showAlert(title: "Email Adderss", message: "Please enter your correct email address", vc: self)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension GR_ForgetPasswordController {
    
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowForResizing), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideForResizing), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: window.origin.y + window.height - keyboardSize.height)
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
    }
    
}

extension GR_ForgetPasswordController: NVActivityIndicatorViewable {
    
    func forgotPassword(email: String) {
        
        let parameters = [
            "email": email,
            "locale": "en"
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(UserForgotPassword, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["message"].string != nil {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["message"].string ?? "Please try again after few moments!", vc: self)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                if response.result.error?.localizedDescription == "JSON could not be serialized because of error:\nThe data couldn’t be read because it isn’t in the correct format." {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: "Please try again after some time!", vc: self)
                        self.stopAnimating()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                        self.stopAnimating()
                    })
                }
            }
            
        }
        
    }
    
}
