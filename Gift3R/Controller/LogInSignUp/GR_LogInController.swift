//
//  GR_LogInController.swift
//  Gift3R
//
//  Created by Ranjit on 14/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.


import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class GR_LogInController: UIViewController {
    
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var passwordField: UITextField!

    var alertPopUp =  UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//      emailAddress.text = "indeedseo@gmail.com"
      emailAddress.text = "atirkas@gmail.com"
      passwordField.text = "654321"
        
      moveInApp()
    }
    
    func moveInApp() {
        if let session = UserDefaults.standard.value(forKey: "SessionId") as? String {
            if session != "" {
                let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "Container") as! Container
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    
    @IBAction func forgetPasswordAction(_ sender: UIButton) {
        removeForgotPasswordPopUp()
    }

    @IBAction func logInAction(_ sender: UIButton) {
        guard let email = emailAddress.text, email != "" else {
            GlobalMethod.init().showAlert(title: "Email Adderss", message: "Please enter your email address!", vc: self)
            return
        }
        
        guard let password = passwordField.text, password != "" else {
            GlobalMethod.init().showAlert(title: "Password", message: "Please enter your password!", vc: self)
            return
        }
        
        if email.isValidEmail {
            loginWithApi(emailAdderss: email, password: password)
        } else {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your correct email Address!", vc: self)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension GR_LogInController: ForgotPasswordProtocol {
    
    func removeForgotPasswordPopUp() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "RegisterSignIn", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "GR_ForgetPassword") as? GR_ForgetPasswordController {
                loadVC.forgotPasswordProtocolDelegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
}

extension GR_LogInController: NVActivityIndicatorViewable {
    
    func loginWithApi(emailAdderss: String, password: String) {
        
        let FCM = UserDefaults.standard.value(forKey: "FCM_Token") ?? ""
        let deviceId = UserDefaults.standard.value(forKey: "deviceId") ?? ""

        let parameters = [
            "email": emailAdderss,
            "password": password,
            "device_type": "iphone",
            "device_id": deviceId,
            "device_token": FCM
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
       startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(Login, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
                
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Login Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["session_id"].string != nil {
                    
                    UserDefaults.standard.set(JSON(res)["session_id"].string, forKey: "SessionId")
                    
                    let profile = JSON(res)["profile"].dictionaryValue
                    
                    UserDefaults.standard.set(profile["email"]?.string, forKey: "Email")
                    UserDefaults.standard.set(profile["phone_number"]?.string, forKey: "PhoneNimber")
                    UserDefaults.standard.set(profile["name"]?.string, forKey: "Name")
                    UserDefaults.standard.set(profile["user_id"]?.int, forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "Container") as! Container
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        self.emailAddress.text = nil
                        self.passwordField.text = nil
                    })
                } else {

                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}
