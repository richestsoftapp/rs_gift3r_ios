//
//  GR_OTPVerifaction.swift
//  Gift3R
//
//  Created by Ranjit on 23/10/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CountryList


class GR_OTPVerifaction: UIViewController {
    
    @IBOutlet weak var otpField: UITextField!

    var userInformations: NSMutableDictionary?
    

    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        
        guard let otp = otpField.text, otp != "" else {
            GlobalMethod.init().showAlert(title: "OTP", message: "Please enter OTP code", vc: self)
            return
        }
        
        guard
            let name = userInformations?.value(forKey: "name") as? String,
            let email = userInformations?.value(forKey: "email") as? String,
            let countryCode = userInformations?.value(forKey: "countryCode") as? String,
            let phone = userInformations?.value(forKey: "mobileNumber") as? String,
            let pass = userInformations?.value(forKey: "password") as? String
        else{
            return
        }
        
        verifyOTP(email: email, password: pass, number: phone, country: countryCode, userName: name, otp: otp)
    }
    

}

extension GR_OTPVerifaction: NVActivityIndicatorViewable {
    
    func verifyOTP(email: String, password: String, number: String, country: String, userName: String, otp: String) {
        
        
        let FCM = UserDefaults.standard.value(forKey: "FCM_Token") ?? ""
        let deviceId = UserDefaults.standard.value(forKey: "deviceId") ?? ""

        
        let parameters = [
            "email": email,
            "country_code": "+1", // country
            "phone_number": number,
            "name": userName,
            "password": password,
            "otp": otp,
            "device_type": "iphone",
            "device_id": deviceId,
            "device_token": FCM
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(Signup, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if let sessionId = JSON(res)["session_id"].string {
                    
                    UserDefaults.standard.set(sessionId, forKey: "SessionId")
                    
                    let profile = JSON(res)["profile"].dictionaryValue
                    
                    UserDefaults.standard.set(profile["email"]?.string, forKey: "Email")
                    UserDefaults.standard.set(profile["phone_number"]?.string, forKey: "PhoneNimber")
                    UserDefaults.standard.set(profile["name"]?.string, forKey: "Name")
                    UserDefaults.standard.set(profile["user_id"]?.int, forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "Container") as! Container
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                if response.result.error?.localizedDescription == "JSON could not be serialized because of error:\nThe data couldn’t be read because it isn’t in the correct format." {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: "Please enter your correct OTP code!", vc: self)
                        self.stopAnimating()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                        self.stopAnimating()
                    })
                }
            }
        }
    }
    
}
