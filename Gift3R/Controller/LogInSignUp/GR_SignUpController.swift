//
//  GR_SignUpController.swift
//  Gift3R
//
//  Created by Ranjit on 14/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import CountryList

class GR_SignUpController: UIViewController, CountryListDelegate {

    @IBOutlet weak var selectedCountryLbl: UILabel!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var number: UITextField!
    @IBOutlet weak var password: UITextField!
    

    var countryList = CountryList()
    
    var registerInfo: NSMutableDictionary?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        countryList.delegate = self
    }

    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryCodeAction(_ sender: UIButton) {
        let navController = UINavigationController(rootViewController: countryList)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func verifyBtnAction(_ sender: UIButton) {
        
        guard let userName = name.text, userName != "" else {
            GlobalMethod.init().showAlert(title: "User Name", message: "Please enter your user name!", vc: self)
            return
        }
    
        guard let emailAddress = email.text, emailAddress != "" else {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your email Address!", vc: self)
            return
        }
        
        guard let countryCode = selectedCountryLbl.text, countryCode != "Code" else {
            GlobalMethod.init().showAlert(title: "Country Code", message: "Please select your country code!", vc: self)
            return
        }
        
        guard let phoneNumber = number.text, phoneNumber != "" else {
            GlobalMethod.init().showAlert(title: "Phone Number", message: "Please enter your mobile number!", vc: self)
            return
        }
        
        guard let passwordValue = password.text, passwordValue != "" else {
            GlobalMethod.init().showAlert(title: "Password", message: "Please enter your password!", vc: self)
            return
        }
        
        if emailAddress.isValidEmail {
            signUp(email: emailAddress, password: passwordValue, number: phoneNumber, country: countryCode, userName: userName)
            registerInfo = ["name": userName, "email": emailAddress, "countryCode": countryCode, "mobileNumber": phoneNumber, "password": passwordValue]
        } else {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your correct email Address!", vc: self)
        }
        
    }
    
    func selectedCountry(country: Country) {
        self.selectedCountryLbl.text = "+\(country.phoneExtension)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension GR_SignUpController: NVActivityIndicatorViewable {
    
    func signUp(email: String, password: String, number: String, country: String, userName: String) {
        
//        let parameters = [
//            "country_code": email,
//            "phone_number": country,
//            "email": number
//        ]
        
        let parameters = [
            "country_code": "+1", //country
            "phone_number": number,
            "email": email
        ]        
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
        */
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(SendOTP, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["message"].string != nil {
                    
                    DispatchQueue.main.async(execute: {
                        let storyBoard = UIStoryboard.init(name: "RegisterSignIn", bundle: nil)
                        let controller = storyBoard.instantiateViewController(withIdentifier: "GR_OTPVerifaction_id") as! GR_OTPVerifaction
                        controller.userInformations = self.registerInfo
                        self.navigationController?.pushViewController(controller, animated: true)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}

