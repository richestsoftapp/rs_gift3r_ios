//
//  MyContactsController.swift
//  Gift3R
//
//  Created by Ranjit on 18/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit
import Contacts

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class MyContactsController: UIViewController, NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noContactLBl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!

    private var searchActive : Bool = false
    
    var dataArray = NSMutableArray()
    var number: [String] = []
    var name:[String] = []
    var AllImages:[String] = []
    
    var userInfo = NSMutableArray()
    var mainArr = NSMutableArray()
    var mainfilteredArr = NSMutableArray()
    var setInfoInTableView = NSMutableArray()
    
    var arr = NSMutableArray()

    var totalArray = NSMutableArray()
    
    
    var startPoint:Int = 0
    var endPoint:Int = 10
    var remainValue: Int = 0
    

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        refreshControl.tintColor = #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.rowHeight = UITableView.automaticDimension
        tableView?.estimatedRowHeight = 89
        
        self.tableView?.addSubview(self.refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        
        if mainArr.count == 0 {
            dataArray.removeAllObjects()
            userInfo.removeAllObjects()
            number.removeAll()
            name.removeAll()
            mainArr.removeAllObjects()
            getAllContact()
        }
        
    }
    
    func getAllContact() {
        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactImageDataKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
                
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        
                        var imgItem: UIImage?
                        
                        if let imageData = contact.imageData {
                            print("image \(String(describing: UIImage(data: imageData)))")
                            imgItem = UIImage(data: imageData)
                            //self.AllImages.append("\(String(describing: imgItem)))")
                            self.AllImages.append("user_icon")

                        } else {
                            print("No image available")
                            imgItem = UIImage(named: "user_icon")
                            self.AllImages.append("user_icon")
                        }
                        
                        let info: [String : Any] = ["name":contact.givenName, "number":number.stringValue, "email":contact.emailAddresses, "img":imgItem!]
                        
                        self.number.append(number.stringValue)
                        self.name.append(contact.givenName)
                        self.userInfo.add(info)
                    }
                    
                }
                
            }
            
            loadContactByApi()
            
        } catch {
            print("unable to fetch contacts")
            
        }
        
    }

}

extension MyContactsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if searchActive {
            
            if mainfilteredArr.count == 0 {
                noContactLBl.isHidden = false
                tableView.isHidden = true
            } else {
                noContactLBl.isHidden = true
                tableView.isHidden = false
            }
            return mainfilteredArr.count

        } else {
            
            if mainArr.count == 0 {
                noContactLBl.isHidden = false
                tableView.isHidden = true
            } else {
                noContactLBl.isHidden = true
                tableView.isHidden = false
            }
            return mainArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContectLCell", for: indexPath) as! ContectLCell
        
        if searchActive {
            
            if let index = mainfilteredArr[indexPath.row] as? [String: Any] {
                cell.username.text = index["name"] as? String
                cell.number.text = index["number"] as? String
                
                let data  =  JSON(index)
                let img = data["img"].stringValue
                if img == "user_icon" {
                    cell.profileImage.image = UIImage(named: "user_icon")
                } else {
                    let imageData = img.data(using: String.Encoding.utf8)
                    cell.profileImage.image = UIImage(data: imageData!)
                }
                
                let ststus = index["status"] as? String
                
                if ststus == "1" {
                    cell.inviteBtn.isHidden = true
                } else {
                    cell.inviteBtn.isHidden = false
                }
                
                cell.inviteBtn.addTarget(self, action: #selector(self.inviteAction(_ :)), for: .touchUpInside)
                cell.inviteBtn.tag = indexPath.row
            }
            
        } else {
            
            print(mainArr)
            
            if let index = mainArr[indexPath.row] as? [String: Any] {
                cell.username.text = index["name"] as? String
                cell.number.text = index["number"] as? String
                
                print(index["img"] ?? "")
                
                
                let data  =  JSON(index)
                let img = data["img"].stringValue
                if img == "user_icon" {
                    cell.profileImage.image = UIImage(named: "user_icon")
                } else {
                    let imageData = img.data(using: String.Encoding.utf8)
                    cell.profileImage.image = UIImage(data: imageData!)
                }
                
                let ststus = index["status"] as? String
                
                if ststus == "1" {
                    cell.inviteBtn.isHidden = true
                } else {
                    cell.inviteBtn.isHidden = false
                }
                
                cell.inviteBtn.addTarget(self, action: #selector(self.inviteAction(_ :)), for: .touchUpInside)
                cell.inviteBtn.tag = indexPath.row
                
                
                if indexPath.row ==  mainArr.count - 1 {
                    
                    if String(remainValue).contains("-") {
                        
                        print(remainValue)
                        
                    } else {
                        let Rnumber = self.setInfoInTableView
                        let totalArrCount = Rnumber.count
                        
                        if self.endPoint > totalArrCount {

                        } else {
                        
                            print(self.startPoint)
                            print(self.endPoint)
                            
                            for i in self.startPoint..<self.endPoint {
                                let info = Rnumber[i]
                                self.mainArr.add(info)
                            }
                            
                            self.remainValue = remainValue - 10
                            print(self.remainValue)
                            
                            if self.remainValue < 10 {
                                self.startPoint = self.endPoint
                                print(self.startPoint)
                                
                                print(self.remainValue)
                                
                                self.endPoint = self.endPoint + self.remainValue
                                
                                print(self.endPoint)
                                
                            }else {
                                self.startPoint = self.endPoint
                                self.endPoint = self.endPoint + 10
                            }
                            tableView.reloadData()
                        }
                    
                    }
                    
                }
                
                
            }
            
        }
        
        return cell
    }
    
    @objc func inviteAction(_ sender : AnyObject) -> Void {        
       
        
        let refreshAlert = UIAlertController(title: "Alert!", message: "Are you sure want to send this link: ", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            if self.searchActive {
                if let index = self.mainfilteredArr[sender.tag] as? [String: Any] {
                    if let num = index["number"] as? String {
                        self.inviteByApi(mobile: num)
                    }
                }
            } else {
                if let index = self.mainArr[sender.tag] as? [String: Any] {
                    if let num = index["number"] as? String {
                        self.inviteByApi(mobile: num)
                    }
                }
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if searchActive {
            if let index = self.mainfilteredArr[indexPath.row] as? [String: Any] {
                
                let ststus = index["status"] as? String
                
                if ststus == "1" {
                    let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "OtherProfileController") as! OtherProfileController
                    vc.information = index
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                }
            }
            
        } else {
            if let index = self.mainArr[indexPath.row] as? [String: Any] {
                
                let ststus = index["status"] as? String
                
                if ststus == "1" {
                    let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                    let vc = storyBoard.instantiateViewController(withIdentifier: "OtherProfileController") as! OtherProfileController
                    vc.information = index
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                } else {
                    
                }
            }
        }
        
    }
    
}

extension MyContactsController {
    
    func loadContactByApi() {
        
        print(AllImages)
        
        let parameters = [
            "addresslist": AllImages,
            "namelist": name,
            "number": number
            ] as [String : Any]
        
        print(parameters)
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(getAllContactList, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                print("Responce => \(JSON(res))")
                
                let namelist = JSON(res)["namelist"].arrayValue
                let addresslist = JSON(res)["addresslist"].arrayValue
                let status = JSON(res)["status"].arrayValue
                let Rnumber = JSON(res)["number"].arrayValue
                
                
                var index = 0
                let mainArr = NSMutableArray()
                
                for _ in namelist {
                    let _name =  namelist[index]
                    let _image =  addresslist[index]
                    let _status =  status[index]
                    let _Rnumber =  Rnumber[index]
                    
                    let data = ["name": "\(_name)","img":_image,"status":"\(_status)","number":"\(_Rnumber)"] as [String : Any]
                    
                    mainArr.add(data)
                    
                    index = index + 1
                    
                }

                let sortedArray = mainArr.sorted{JSON($0)["name"].stringValue < JSON($1)["name"].stringValue}
                
                for i in sortedArray {
                    self.setInfoInTableView.add(i)
                }
                
                if self.setInfoInTableView.count < 10 {
                    self.mainArr = self.setInfoInTableView
                   
                } else {
                    
                    for i in self.startPoint..<self.endPoint {
                        let info = self.setInfoInTableView[i]
                        self.mainArr.add(info)
                    }
                    
                    self.startPoint = self.endPoint
                    self.endPoint = self.endPoint + 10
                    
                    self.remainValue = self.setInfoInTableView.count  - 10
                   
                }
                
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
                
                
                print(self.setInfoInTableView)
               
                DispatchQueue.main.async(execute: {
                     self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
            
        }
        
    }
    
    func loadContactByApi1() {
        
        let parameters = [
            "addresslist": AllImages,
            "namelist": name,
            "number": number
            ] as [String : Any]
        
        
        Alamofire.request(getAllContactList, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.refreshControl.endRefreshing()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                let namelist = JSON(res)["namelist"].arrayValue
                let addresslist = JSON(res)["addresslist"].arrayValue
                let status = JSON(res)["status"].arrayValue
                let Rnumber = JSON(res)["number"].arrayValue
                
                self.mainArr.removeAllObjects()
                self.setInfoInTableView.removeAllObjects()
                
                var index = 0
                let mainArr = NSMutableArray()
                
                for _ in namelist {
                    let _name =  namelist[index]
                    let _image =  addresslist[index]
                    let _status =  status[index]
                    let _Rnumber =  Rnumber[index]
                    
                    let data = ["name": "\(_name)","img":_image,"status":"\(_status)","number":"\(_Rnumber)"] as [String : Any]
                    mainArr.add(data)
                    index = index + 1
                }
                
                let sortedArray = mainArr.sorted{JSON($0)["name"].stringValue < JSON($1)["name"].stringValue}
                
                for i in sortedArray {
                    self.setInfoInTableView.add(i)
                }
                
                if self.setInfoInTableView.count < 10 {
                    self.mainArr = self.setInfoInTableView
                } else {
                    
                    for i in self.startPoint..<self.endPoint {
                        let info = self.setInfoInTableView[i]
                        self.mainArr.add(info)
                    }
                    
                    self.startPoint = self.endPoint
                    self.endPoint = self.endPoint + 10
                    
                    self.remainValue = self.setInfoInTableView.count - 10
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
                
                print(self.setInfoInTableView)
                
                DispatchQueue.main.async(execute: {
                    self.refreshControl.endRefreshing()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.refreshControl.endRefreshing()
                })

            }
        }
    }
    
}


extension MyContactsController {
    
    func inviteByApi(mobile: String) {
        
        var val: String = ""
        
        for i in  mobile {
            
            if i != " " {
                val.append(i)
            }
            
        }
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        let parameters = [
            "phone_number": val,
            "session_id": sessionId
        ]
        
        print("Prams => \(parameters)")
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(inviteByUrl, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["message"].string == "successfully send link" {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Success!", message:"A Invitation link has been sent to this \(mobile)" , vc: self)
                    }
                    
                    self.userInfo.removeAllObjects()
                    self.dataArray.removeAllObjects()
                    self.number.removeAll()
                    self.name.removeAll()
                    
                    self.loadContactByApi()
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["message"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
    func setupUI() {
        
    }
    
}


/* Search by city */
extension MyContactsController : UISearchDisplayDelegate, UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
        tableView?.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        
        if mainfilteredArr.count == 0 {
            searchActive = false;
            tableView?.reloadData()
        } else {
            searchActive = true
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let info = setInfoInTableView.filter { (item) -> Bool in
            // Check the title
            let title = JSON(item)["name"].stringValue
            let num = JSON(item)["number"].stringValue

            if title.range(of: searchText, options: .caseInsensitive) != nil {
                return true
            } else if num.range(of: searchText, options: .caseInsensitive) != nil {
                return true
            } else {
                return false
            }
        }
        
        if info.count == 0 {
            mainfilteredArr.removeAllObjects()
        } else {
            mainfilteredArr.removeAllObjects()
            for i in info {
                mainfilteredArr.add(i)
            }
        }
    
        tableView.reloadData()
        searchActive = true
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
    
        searchActive = false
        
        
        dataArray.removeAllObjects()
        userInfo.removeAllObjects()
        number.removeAll()
        name.removeAll()
    
       // mainArr.removeAllObjects()
        
        
        self.startPoint = 0
        self.endPoint = 10
        self.remainValue = 0
        
        getAllContact1()
        
    }
    
    func getAllContact1() {
        
        let contactStore = CNContactStore()
        var contacts = [CNContact]()
        let keys = [
            CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
            CNContactPhoneNumbersKey,
            CNContactEmailAddressesKey,
            CNContactImageDataKey
            ] as [Any]
        let request = CNContactFetchRequest(keysToFetch: keys as! [CNKeyDescriptor])
        do {
            try contactStore.enumerateContacts(with: request){
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                contacts.append(contact)
                
                for phoneNumber in contact.phoneNumbers {
                    if let number = phoneNumber.value as? CNPhoneNumber, let label = phoneNumber.label {
                        
                        var imgItem: UIImage?
                        
                        if let imageData = contact.imageData {
                            print("image \(String(describing: UIImage(data: imageData)))")
                            imgItem = UIImage(data: imageData)
                            //self.AllImages.append("\(String(describing: imgItem)))")
                            self.AllImages.append("user_icon")
                            
                        } else {
                            print("No image available")
                            imgItem = UIImage(named: "user_icon")
                            self.AllImages.append("user_icon")
                        }
                        
                        let info: [String : Any] = ["name":contact.givenName, "number":number.stringValue, "email":contact.emailAddresses, "img":imgItem!]
                        
                        self.number.append(number.stringValue)
                        self.name.append(contact.givenName)
                        self.userInfo.add(info)
                    }
                    
                }
                
            }
            
            loadContactByApi1()
            
        } catch {
            print("unable to fetch contacts")
            
        }
        
    }
    
    
}



