//
//  OtherProfileController.swift
//  Gift3R
//
//  Created by Ranjit on 21/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class OtherProfileController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var emptyMsgLbl: UILabel!

    
    var information: [String: Any] = [:]
    
    var dataSource = [Stores]()
    
    var mobile: String?
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        refreshControl.tintColor = #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1)
        
        return refreshControl
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.addSubview(self.refreshControl)

        if  let phone = information["number"] as? String {
            getAllItems(mobileNum: phone, loader: "1")
        }
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}


extension OtherProfileController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 25, height: 208)
    }
}

extension OtherProfileController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if dataSource.count == 0 {
            emptyMsgLbl.isHidden = false
        } else {
            emptyMsgLbl.isHidden = true
        }
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OtherProfileItemCell", for: indexPath) as! OtherProfileItemCell

        cell.setUpCell(info: dataSource[indexPath.row])

        return cell
    }
    
}

extension OtherProfileController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailController
        vc.storeListAndDetails = dataSource[indexPath.row]
        vc.isComeFrom = "OtherUserFav"
        vc.phoneContactList = mobile
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}


extension OtherProfileController: NVActivityIndicatorViewable {
    
    func getAllItems(mobileNum: String, loader: String) -> Void {
        
        if loader == "1" {
            startAnimating(CGSize(width: 60, height: 60), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.8900398612, green: 0.238706708, blue: 0.194413811, alpha: 1))
        }
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "phone_number":mobileNum
        ]
    
        print("Prams => \(parameters)")
        
        Alamofire.request(getAllOtherUsersItem, method:.post, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async { self.stopAnimating()
                        self.refreshControl.endRefreshing()
                    }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].stringValue == "Session Expired" {
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                }
                
                if JSON(res)["error_description"].stringValue == "No Record Found." {
                    
                } else {
                    let profile = JSON(res)["profile"].dictionaryValue
                    DispatchQueue.main.async(execute: {
                        self.userName.text = profile["name"]?.stringValue
                        self.mobile = profile["phone_number"]?.stringValue
                        self.mobileNumber.text = profile["phone_number"]?.stringValue
                    })
                    
                    if JSON(res)["favouritelist"].arrayValue.count != 0 {
                        for i in JSON(res)["favouritelist"].arrayValue {
                            self.dataSource.append(Stores.init(info: i))
                        }
                    }
                }
        
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                    self.collectionView.reloadData()
                    self.refreshControl.endRefreshing()
                })
                
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                  //  GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                    self.refreshControl.endRefreshing()
                })
            }
        }
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
    
        if  let phone = information["number"] as? String {
            getAllItems(mobileNum: phone, loader: "0")
        }
    }
    
    
}

