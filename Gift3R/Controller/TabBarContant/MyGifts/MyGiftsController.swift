//
//  MyGiftsController.swift
//  Gift3R
//
//  Created by Ranjit on 15/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


import ContactsUI


class MyGiftsController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyMsgLbl: UILabel?
    
    private var giftsList: [Gifts] = []
    private  var searchedGiftsList: [Gifts] = []
    private var searchActive : Bool = false
    
    private var char: String?
    
    var cardId: String?
    
    var alertPopUp =  UIViewController()
    
    var dataForSend: Gifts?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        refreshControl.tintColor = #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1)
        
        return refreshControl
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView?.addSubview(self.refreshControl)
        getMyGifts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchActive = false
        getMyGifts2()
    }
    
}


extension MyGiftsController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if searchActive {
            if searchedGiftsList.count == 0 {
                emptyMsgLbl?.isHidden = false
            } else {
                emptyMsgLbl?.isHidden = true
            }
            return searchedGiftsList.count
        } else {
            if giftsList.count == 0 {
                emptyMsgLbl?.isHidden = false
            } else {
                emptyMsgLbl?.isHidden = true
            }
            return giftsList.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GR_MyGiftsCell_Id", for: indexPath) as! GR_MyGiftsCell
        cell.delegate = self
        if searchActive {
            cell.setUpInfo(info: searchedGiftsList[indexPath.row])
        } else {
            cell.setUpInfo(info: giftsList[indexPath.row])
        }
        
        return cell
    }
    
}

extension MyGiftsController: GR_MyGiftsProtocal {
    func messageBtn(cell: GR_MyGiftsCell) {
        
        guard let indexPath = self.tableView.indexPath(for: cell)
            else{
                return
        }
        
        if searchActive {
            cardId = searchedGiftsList[indexPath.row].giftCardId
            dataForSend = searchedGiftsList[indexPath.row]
            //  self.present(contactPicker, animated: true, completion: nil)
        } else {
            cardId = giftsList[indexPath.row].giftCardId
            dataForSend = giftsList[indexPath.row]
            // self.present(contactPicker, animated: true, completion: nil)
        }
        
        
        if dataForSend?.message == "" {
            GlobalMethod().showAlert(title: "No message found!", message: "", vc: self)
            return
        } else {
            
            if !(alertPopUp.isViewLoaded) {
                let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
                if let loadVC = storyboard.instantiateViewController(withIdentifier: "ReadMessagePopUp") as? ReadMessagePopUp {
                    loadVC.delegate = self
                    loadVC.message = dataForSend?.message
                    self.addChild(loadVC)
                    self.view.addSubview(loadVC.view)
                    alertPopUp.view.alpha = 0
                    UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                        self.alertPopUp.view.alpha = 1
                    }, completion: nil)
                    
                    alertPopUp = loadVC
                }
            }
        }
        
    }
    
    
    func info(cell: GR_MyGiftsCell) {
        guard let indexPath = self.tableView.indexPath(for: cell)
            else{
                return
        }
        
        if searchActive {
           let index = searchedGiftsList[indexPath.row]
            GlobalMethod().showAlert(title: "", message: "? GIFT3Rapp Electronic Gift Cards, cary a balance and can be used on multiple visits till the balance is zero. Your eGift Cards never expire. eGift Cards are not re-loadable.", vc: self)
            
        } else {
            let index = giftsList[indexPath.row]
            GlobalMethod().showAlert(title: "", message: "? GIFT3Rapp Electronic Gift Cards, cary a balance and can be used on multiple visits till the balance is zero. Your eGift Cards never expire. eGift Cards are not re-loadable.", vc: self)
            
        }
        print(indexPath.row)
    }
    
    func redeem(cell: GR_MyGiftsCell) {
        guard let indexPath = self.tableView.indexPath(for: cell)
            else {
                return
        }
        if searchActive {
            let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "RedeemCardController") as! RedeemCardController
            vc.info  = searchedGiftsList[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "RedeemCardController") as! RedeemCardController
            vc.info  = giftsList[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        print(indexPath.row)
    }
    
    func send(cell: GR_MyGiftsCell) {
        guard let indexPath = self.tableView.indexPath(for: cell)
            else {
                return
        }
        
        if searchActive {
            cardId = searchedGiftsList[indexPath.row].giftCardId
            dataForSend = searchedGiftsList[indexPath.row]
          //  self.present(contactPicker, animated: true, completion: nil)
        } else {
            cardId = giftsList[indexPath.row].giftCardId
            dataForSend = giftsList[indexPath.row]
           // self.present(contactPicker, animated: true, completion: nil)
        }
        
        remove()
        
        print(indexPath.row)
    }
    
}

extension MyGiftsController: NVActivityIndicatorViewable{
    
    private func getMyGifts() {
        
        startAnimating(CGSize(width: 60, height: 60), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.8900398612, green: 0.238706708, blue: 0.194413811, alpha: 1))
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": "",
            "locale": ""
        ]
        
        Alamofire.request(MyCards, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async { self.stopAnimating() }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["error_description"].string == nil {
                    
                    self.giftsList.removeAll()
                    
                    for i in JSON(res).arrayValue {
                        self.giftsList.append(Gifts.init(info: i))
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
        
    }
    
    private func getMyGifts2() {
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": "",
            "locale": ""
        ]
        
        Alamofire.request(MyCards, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].string == nil {
                    
                    self.giftsList.removeAll()
                    
                    for i in JSON(res).arrayValue {
                        self.giftsList.append(Gifts.init(info: i))
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    //GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
}

/* Search by city */
extension MyGiftsController: UISearchDisplayDelegate, UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
        tableView?.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = true;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        char = searchText
        searchByApi(char: searchText)
        searchActive = true;
    }
    
}


extension MyGiftsController {
    
    private func searchByApi(char: String)  {
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": char,
            ]
        
        print("Prams => \(parameters)")
        
        Alamofire.request(MyCards, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].string == nil {
                    
                    self.searchedGiftsList = []
                    
                    for i in JSON(res).arrayValue{
                        self.searchedGiftsList.append(Gifts.init(info: i))
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.tableView.reloadData()
                    })
                    
                } else {
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                })
            }
        }
        
    }

}



extension MyGiftsController: SendGiftMessageProtocal {
    
    func remove() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "SendGiftMessageController") as? SendGiftMessageController {
                loadVC.delegate = self
                loadVC.info = dataForSend
                
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
    
}


extension MyGiftsController: ReadMessagePopUpProtocal {
    
    func removeReadMessagePopUp() {
        
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        
    }
    
    

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": "",
            "locale": ""
        ]
        
        Alamofire.request(MyCards, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            print(response)
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.refreshControl.endRefreshing()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].string == nil {
                    
                    self.giftsList.removeAll()
                    
                    for i in JSON(res).arrayValue {
                        self.giftsList.append(Gifts.init(info: i))
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    //GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.refreshControl.endRefreshing()
                })
            }
        }
    }
    
    
}
