
//
//  PaymentConfirmationController.swift
//  Gift3R
//
//  Created by Ranjit on 05/01/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class PaymentConfirmationController: UIViewController {
    
    @IBOutlet weak var totalAmount: UITextField!
    @IBOutlet weak var partiallyAmount: UITextField!
    @IBOutlet weak var partiallyViewHeight: NSLayoutConstraint!
    @IBOutlet weak var paetiallyView: UIView!
    
    var info: Gifts?
    
    var amount: String?
    var receiptNo: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(receiptNo)
        print(amount)

        partiallyViewHeight.constant = 0
        
        if let amount = info?.availableBalance {
            let formatter = NumberFormatter()
            formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
            formatter.numberStyle = .currency
            if let formattedTipAmount = formatter.string(from: Double(amount)! as NSNumber) {
                totalAmount.text = "\(formattedTipAmount)"
                partiallyAmount.text = "\(formattedTipAmount)"
            }
        }
        
        paetiallyView.isHidden = true
        
        partiallyAmount.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)

    }
    
    
    @objc func myTextFieldDidChange(_ textField: UITextField) {
//        var text = partiallyAmount.text?.replacingOccurrences(of: "$", with: "")
//        text = "$" + text!
//        partiallyAmount.text = text
        
        if let amountString = textField.text?.currencyInputFormatting() {
            partiallyAmount.text = amountString
        }
        
    }
    
    
    @IBAction func wantToMakePartialPaymentAction(_ sender: Any) {
        partiallyViewHeight.constant = 120
        paetiallyView.isHidden = false
    }
    
    
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deduct(_ sender: Any) {

        guard let partiallyAmount = self.partiallyAmount.text, partiallyAmount != "" else {
            GlobalMethod.init().showAlert(title: "Alert", message: "Please enter the partially amount!", vc: self)
            return
        }
        
        let price = removeSpecialCharsFromString(text: partiallyAmount)
        
        var priceVal: String = ""
        
        for i in price {
            
            if i != "," {
                priceVal.append(i)
            }
            
        }
        

        if let int = Double(priceVal), let val = Double(info?.availableBalance ?? "") {
            
            if int <= val {
                
                let dialogMessage = UIAlertController(title: "Alert", message: "You are about to deduct this \(partiallyAmount) amount from your eGift Card?", preferredStyle: .alert)
                
                // Create OK button with action handler
                let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    guard let receipt = self.receiptNo else { return }
                    self.paymentConfirmation(receiptNumber: receipt, amount: priceVal)
                    
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
                
            } else {
                GlobalMethod.init().showAlert(title: "Alert", message: "Your partially amount is gratter then total amount!", vc: self)
            }
            
        }
        
        
    }
    
}

extension PaymentConfirmationController: NVActivityIndicatorViewable {
    
    func removeSpecialCharsFromString(text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+-*=(),.:!_".characters)
        return String(text.characters.filter {okayChars.contains($0) })
    }
    
    func paymentConfirmation(receiptNumber: String, amount: String) {
        
        guard let billAmount = self.amount else { return }

        let price = removeSpecialCharsFromString(text: billAmount)
    
        print(price)
        
        var priceVal: String = ""
        
        for i in price {
            
            if i != "," {
                priceVal.append(i)
            }
            
        }
        
        print(priceVal)
        

        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        guard let id = info?.giftCardId  else {
            return
        }
        
        guard let mypurchasedcardId = info?.myPurchasedcardId  else {
            return
        }
        
        
        let parameters = [
            "session_id": sessionId,
            "card_id": id,
            "receipt_number": receiptNumber,
            "bill_amount": priceVal,
            "amount": amount,
            "mypurchasedcard_id": mypurchasedcardId
        ]
        
        print("Prams => \(parameters)")
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
        */
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(RedeemCard, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["message"].stringValue == "Amount redeemed to merchant" {
                    
                    DispatchQueue.main.async {
                        let refreshAlert = UIAlertController(title: "Success", message: JSON(res)["message"].stringValue, preferredStyle: UIAlertController.Style.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                            
                            let vc = (self.navigationController?.viewControllers[1])
                            self.navigationController?.popToViewController(vc!, animated: true)

                            
                        }))
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                    
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}
