//
//  RedeemCardController.swift
//  Gift3R
//
//  Created by Ranjit on 05/01/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class RedeemCardController: UIViewController {
    
    
    @IBOutlet weak var titleOfAleart: UILabel!
    
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var receiptNumber: MaxLengthTextField!
    
    var info: Gifts?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleOfAleart.text = "Please, enter the total bill amount located on your receipt (ie. $20.57). Make sure the store manager/server is present when you do this."
        
        amount.addTarget(self, action: #selector(myTextFieldDidChange), for: .editingChanged)
    }
    
    @objc func myTextFieldDidChange(_ textField: UITextField) {
        
//        var text = amount.text?.replacingOccurrences(of: "$", with: "")
//        text = "$" + text!
//        amount.text = text
        
        if let amountString = textField.text?.currencyInputFormatting() {
            amount.text = amountString
        }
        
    }
  
    
    @IBAction func nextButton(_ sender: Any) {
        
        guard let amount = amount.text, amount != "$" else {
            GlobalMethod.init().showAlert(title: "Amount", message: "Please enter your amount!", vc: self)
            return
        }
        
        let price = amount.replacingOccurrences(of: "$", with: "")
        
        
        var priceVal: String = ""
        
        for i in price {
            
            if i != "," {
                priceVal.append(i)
            }
            
        }
        
        let p = priceVal.components(separatedBy: ".").first
        
        let actualBalance = info?.availableBalance?.components(separatedBy: ".").first
        
        print(actualBalance)
        
//        if let int = Double(priceVal), let val = Double(info?.availableBalance ?? "") {
//            if int <= val {
                    guard let receiptNo = receiptNumber.text, receiptNo != "" else {
                        GlobalMethod.init().showAlert(title: "Receipt Number", message: "Please enter your receipt number!", vc: self)
                        return
                    }
                    
                    if receiptNo.count == 4 {
                        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
                        let vc = storyBoard.instantiateViewController(withIdentifier: "PaymentConfirmationController") as! PaymentConfirmationController
                        vc.info  = self.info
                        vc.amount = amount
                        vc.receiptNo = receiptNo
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        GlobalMethod.init().showAlert(title: "Receipt Number", message: "Please, enter the last four digits of your current receipt. Ask your server!", vc: self)
                    }
//                } else {
//                GlobalMethod.init().showAlert(title: "Alert", message: "Please enetr valid price!", vc: self)
//            }
//        }
        
    }
    
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension String {
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = "$"
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        
        return formatter.string(from: number)!
    }
    
}
