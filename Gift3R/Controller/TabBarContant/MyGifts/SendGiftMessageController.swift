//
//  SendGiftMessageController.swift
//  Gift3R
//
//  Created by Ranjit on 20/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit


import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


import ContactsUI


protocol SendGiftMessageProtocal {
    func remove()
}

class SendGiftMessageController: UIViewController, CNContactPickerDelegate, UITextViewDelegate {
    
    @IBOutlet weak var sendBtn: DesignableButton!
    
    @IBOutlet weak var headertitle: UILabel!
    @IBOutlet weak var messageView: UITextView!
    
    @IBOutlet weak var boxView: DesignableView!
    let contactPicker = CNContactPickerViewController()

    var delegate: SendGiftMessageProtocal?

    var info: Gifts?
    
    var infoComeFromOtherContact:StoresGift?

    
    fileprivate var placeholderLabel : UILabel!
    
    var redOnly: String?
    var msg: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        contactPicker.delegate = self
        
        switch redOnly {
        case "true":
                messageView.isUserInteractionEnabled = false
                headertitle.text = "READ NOTE"
                
                sendBtn.isHidden = true
                
                if info?.message != "" {
                    messageView.text = info?.message
                } else {
                     messageView.text = "No Message"
                }
   
        case "StoreDetails":
        
            messageView.isUserInteractionEnabled = false
            headertitle.text = "READ NOTE"
            sendBtn.isHidden = true
            
            if msg != "" {
                messageView.text = msg
            } else {
                placeHolderOfTextView()
            }
            
        default:
            placeHolderOfTextView()
        }
        
        
    }
    
    
    func placeHolderOfTextView() -> Void {
        messageView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter a personalized message"
        placeholderLabel.sizeToFit()
        messageView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (messageView.font?.pointSize)! / 3)
        placeholderLabel.textColor = UIColor.gray
        placeholderLabel.isHidden = !messageView.text.isEmpty
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !messageView.text.isEmpty
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        delegate?.remove()
    }
    
    @IBAction func sendBtnAction(_ sender: Any) {

//        guard let msg = messageView.text, msg != "" else {
//            GlobalMethod.init().showAlert(title: "Message", message: "Please enter you message!", vc: self)
//            return
//        }
        
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        self.delegate?.remove()
        
        for number in contact.phoneNumbers {
            
            let phoneNumber = number.value
            
            DispatchQueue.main.async {
                let dialogMessage = UIAlertController(title: "Alert", message: "Are you sure you want to send this card?", preferredStyle: .alert)
                
                // Create OK button with action handler
                let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    guard let id = self.info?.giftCardId else { return }
                    guard let myPurchId = self.info?.myPurchasedcardId else { return }
                    self.sendbyApi(number: phoneNumber.stringValue, id: id, myPurchasedcardId: myPurchId)
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
            }
            
        }
        
    }
    
    
}

extension SendGiftMessageController: NVActivityIndicatorViewable {
    
    func sendbyApi(number: String, id: String, myPurchasedcardId: String) {
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        let parameters = [
            "session_id": sessionId,
            "card_id": id,
            "phone_number": number,
            "message":messageView.text,
            "mypurchasedcard_id":myPurchasedcardId
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(SendMyCards, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["message"].string == "send cards succesfully" {
                    
                    DispatchQueue.main.async {
                        
                        let refreshAlert = UIAlertController(title: "Success", message: "Cards sent succesfully", preferredStyle: UIAlertController.Style.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                            
                            // self.delegate?.remove()
                            
                        }))
                        
                        self.present(refreshAlert, animated: true, completion: nil)
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}
