
//
//  MyRegistryController.swift
//  Gift3R
//
//  Created by Ranjit on 18/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


class MyRegistryController: UIViewController {
    
    @IBOutlet weak var storesCollectionView: UICollectionView!
    
    @IBOutlet weak var emptyMsgLbl: UILabel?
    
    var storeList: [Stores] = []
    
    var pageNum: Int = 1
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        refreshControl.tintColor = #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1)
        
        return refreshControl
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.storesCollectionView?.addSubview(self.refreshControl)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        getStoresList(page: "1")
    }

}

extension MyRegistryController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 25, height: 208)
    }
}

extension MyRegistryController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
            if storeList.count == 0 {
                emptyMsgLbl?.isHidden = false
            } else {
                emptyMsgLbl?.isHidden = true
            }
            return storeList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell_Id", for: indexPath) as! StoreCell
        
            cell.setUpCell(info: storeList[indexPath.row])
        
        return cell
    }
    
}

extension MyRegistryController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailController
            vc.storeListAndDetails = storeList[indexPath.row]
            vc.isComeFrom = "Registry"
            self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension MyRegistryController: NVActivityIndicatorViewable {
    
    func getStoresList(page: String) -> Void {
        
        startAnimating(CGSize(width: 60, height: 60), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.8900398612, green: 0.238706708, blue: 0.194413811, alpha: 1))
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)"
        ]
        
        print("Prams => \(parameters)")
        
        Alamofire.request(MyRegistry, method:.post, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async { self.stopAnimating() }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].stringValue == "Session Expired" {
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                self.storeList.removeAll()
                
                if JSON(res).arrayValue.count != 0 {
                    
                    for i in JSON(res).arrayValue{
                        self.storeList.append(Stores.init(info: i))
                    }
                    self.pageNum = 1
                   
                    
                } else {
                    DispatchQueue.main.async {
                     //   GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.storesCollectionView.reloadData()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    //GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)"
        ]
        
        print("Prams => \(parameters)")
        
        Alamofire.request(MyRegistry, method:.post, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async { self.refreshControl.endRefreshing() }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].stringValue == "Session Expired" {
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.refreshControl.endRefreshing()
                })
                
                self.storeList.removeAll()
                
                if JSON(res).arrayValue.count != 0 {
                    
                    for i in JSON(res).arrayValue{
                        self.storeList.append(Stores.init(info: i))
                    }
                    self.pageNum = 1
                    
                    
                } else {
                    DispatchQueue.main.async {
                        //   GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.storesCollectionView.reloadData()
                    self.refreshControl.endRefreshing()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.refreshControl.endRefreshing()
                })
            }
        }
    }
    
    
}




/*
 
 {
 "email" : "ranjit@richestsoft.in",
 "image" : "http:\/\/18.221.106.68\/gift3r\/public\/thumb\/uploads\/4UT7Mphb_Chrysanthemum.jpg",
 "city" : "werwe",
 "website" : "demo.richest.com",
 "zipcode" : "543543",
 "phone_number" : "85858585858",
 "s_name" : "Ranjit",
 "address" : "defrser",
 "status" : 1,
 "s_id" : 60
 }
 
*/

class Favouritelist: NSObject {
    
    var email: String?
    var image: String?
    var city: String?
    
    var website: String?
    var zipcode: String?
    
    var phoneNumber: String?
    var sName: String?
    
    var address: String?
    
    var status: Int?
    var sId: String?
    
    override init(){
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
    
        email = jsonInfo["email"]?.stringValue
        image = jsonInfo["image"]?.stringValue
        city = jsonInfo["city"]?.stringValue
        website = jsonInfo["website"]?.stringValue
        zipcode = jsonInfo["zipcode"]?.stringValue
        
        phoneNumber = jsonInfo["phone_number"]?.stringValue
        sName = jsonInfo["s_name"]?.stringValue
        address = jsonInfo["address"]?.stringValue
        status = jsonInfo["status"]?.intValue
        sId = jsonInfo["s_id"]?.stringValue
        
    }
    
    
}
