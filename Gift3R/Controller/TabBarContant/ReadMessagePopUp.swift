//
//  ReadMessagePopUp.swift
//  Gift3R
//
//  Created by Ranjit on 28/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

protocol ReadMessagePopUpProtocal {
    func removeReadMessagePopUp()
}

class ReadMessagePopUp: UIViewController {
    
    var delegate: ReadMessagePopUpProtocal?
    
    var message: String?
    
    @IBOutlet weak var messageLBL: UILabel!
    
    
    var tapGesture = UITapGestureRecognizer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // tapGesture.delegate = self
        if let m = message {
            messageLBL.text = m
        }
        tapEvent()
    }
    
    func tapEvent() -> Void {
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.view?.addGestureRecognizer(tapGesture)
        self.view?.isUserInteractionEnabled = true
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        delegate?.removeReadMessagePopUp()
    }
    

}
