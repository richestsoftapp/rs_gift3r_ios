//
//  ChangePasswordController.swift
//  Gift3R
//
//  Created by Ranjit on 17/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

protocol ChangePasswordProtocol {
    func removeForgotPasswordPopUp() -> Void
}

class ChangePasswordController: UIViewController {

    @IBOutlet weak var oldPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    
    var ChangePasswordProtocolDelegate : ChangePasswordProtocol?

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func goBack(_ sender: UIButton) {
        ChangePasswordProtocolDelegate?.removeForgotPasswordPopUp()
    }
    
    
    
    @IBAction func changePasswordAction(_ sender: Any) {
        
        guard let old = oldPassword.text, old != "" else {
            GlobalMethod.init().showAlert(title: "Old Password", message: "Please enter your old password!", vc: self)
            return
        }
       
        guard let new = newPassword.text, new != "" else {
            GlobalMethod.init().showAlert(title: "New Password", message: "Please enter your new password!", vc: self)
            return
        }
        
        guard let confirm = confirmPassword.text, confirm != "" else {
            GlobalMethod.init().showAlert(title: "Confirm Password", message: "Please confirm your new password!", vc: self)
            return
        }
        
        if new == confirm {
            logout(old: old, new: new)
        } else {
            GlobalMethod.init().showAlert(title: "Alert", message: "Password does not match!", vc: self)
        }
    }
    
}


extension ChangePasswordController: NVActivityIndicatorViewable {
    
    func logout(old: String, new: String) {
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "password": old,
            "new_password": new
        ]
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(ChangePassword, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["error_description"].string == nil {
                    
                    DispatchQueue.main.async(execute: {
                        let refreshAlert = UIAlertController(title: "Success", message: "Your password is changed successfully!", preferredStyle: UIAlertController.Style.alert)
                        
                        refreshAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                            self.ChangePasswordProtocolDelegate?.removeForgotPasswordPopUp()
                        }))
                        self.present(refreshAlert, animated: true, completion: nil)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                if response.result.error?.localizedDescription == "JSON could not be serialized because of error:\nThe data couldn’t be read because it isn’t in the correct format." {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: "Please enter your correct OTP code!", vc: self)
                        self.stopAnimating()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async(execute: {
                       // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                        self.stopAnimating()
                    })
                }
            }
        }
    }
    
}

