//
//  SettingsController.swift
//  Gift3R
//
//  Created by Ranjit on 15/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class SettingsController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var email: UILabel!
    
    var alertPopUp =  UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInfo()
    }

    func setupInfo() {
        username.text = UserDefaults.standard.value(forKey: "Name") as? String
        email.text = UserDefaults.standard.value(forKey: "Email") as? String
        mobile.text = UserDefaults.standard.value(forKey: "PhoneNimber") as? String
    }

    @IBAction func changePassword(_ sender: UIButton) {
        removeForgotPasswordPopUp()
    }
    
    @IBAction func termsAndConditionsAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditionsController
        vc.headerTitle = "Terms And Conditions"
        vc.webURL = TermsConditons
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func termsAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditionsController
        vc.headerTitle = "Terms Of Use"
        vc.webURL = TermsOfUse
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func policyPrivacyAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditionsController
        vc.headerTitle = "Policy And Privacy"
        vc.webURL = PolicyPrivacy
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func aboutUsAction(_ sender: UIButton) {
        let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "TermsAndConditions") as! TermsAndConditionsController
        vc.headerTitle = "About Us"
        vc.webURL = AboutUS
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func logOutAction(_ sender: UIButton) {
        
        let refreshAlert = UIAlertController(title: "Alert", message: "Are you sure want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.logout()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension SettingsController: ChangePasswordProtocol {
    func removeForgotPasswordPopUp() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "ChangePassword") as? ChangePasswordController {
                loadVC.ChangePasswordProtocolDelegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
}


extension SettingsController: NVActivityIndicatorViewable {
    
    func logout() {
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)"
        ]
        
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(Logout, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["error_description"].string == nil {
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert!", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                if response.result.error?.localizedDescription == "JSON could not be serialized because of error:\nThe data couldn’t be read because it isn’t in the correct format." {
                    
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: "Please try again in some time!", vc: self)
                        self.stopAnimating()
                    })
                    
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                        self.stopAnimating()
                    })
                }
            }
        }
    }
    
}
