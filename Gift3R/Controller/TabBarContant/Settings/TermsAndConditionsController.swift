//
//  TermsAndConditionsController.swift
//  Gift3R
//
//  Created by Ranjit on 17/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

class TermsAndConditionsController: UIViewController, UIWebViewDelegate{
    
    
    @IBOutlet weak var webView: UIWebView?
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    var isComeFrom: String?
    var webURL: String?
    var headerTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initilization()
    }
    
    
    func initilization() -> Void {
        
        print(isComeFrom ?? "Nil")
        
        titleLbl?.text = headerTitle
        activityIndicator?.startAnimating()
        webView?.delegate = self
        guard let strURL = webURL else { return }
        if let  url = NSURL(string: strURL) {
            let request = NSMutableURLRequest(url: url as URL)
            print(request)
            webView?.loadRequest(request as URLRequest)
        }
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicator?.stopAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator?.stopAnimating()
        activityIndicator?.isHidden = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        activityIndicator?.stopAnimating()
        activityIndicator?.isHidden = true
    }
    
    
    
    @IBAction func goBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
