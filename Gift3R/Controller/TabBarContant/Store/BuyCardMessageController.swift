//
//  BuyCardMessageController.swift
//  Gift3R
//
//  Created by Ranjit on 22/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView


protocol BuyCardMessageProtocal {
    func removeVC()
}

class BuyCardMessageController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var sendBtn: DesignableButton!
    
    @IBOutlet weak var headertitle: UILabel!
    @IBOutlet weak var messageView: UITextView!
    
    
    var delegate: BuyCardMessageProtocal?
    
    var list: StoresGift?
    var shop: Stores?
    
    var number: String?
    

    fileprivate var placeholderLabel : UILabel!
    
    
    var redOnly: String?
    
    var price  = ""
    
    var alertPopUp =  UIViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeHolderOfTextView()
        
        
//        if redOnly == "true" {
//            messageView.isUserInteractionEnabled = false
//            headertitle.text = "READ NOTE"
//            sendBtn.isHidden = true
//            messageView.text = info?.message
//        }
        
    }
    
    
    func placeHolderOfTextView() -> Void {
        messageView.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter a personalized message"
        placeholderLabel.sizeToFit()
        messageView.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (messageView.font?.pointSize)! / 3)
        placeholderLabel.textColor = UIColor.gray
        placeholderLabel.isHidden = !messageView.text.isEmpty
    }
    
    func textViewDidChange(_ textView: UITextView) {
        placeholderLabel.isHidden = !messageView.text.isEmpty
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        delegate?.removeVC()
    }
    
    @IBAction func sendBtnAction(_ sender: Any) {
        
//        guard let msg = messageView.text, msg != "" else {
//            GlobalMethod.init().showAlert(title: "Message", message: "Please enter you message!", vc: self)
//            return
//        }
        
        remove()
        
    }
    
}




extension BuyCardMessageController: PaymentAcceptProtocal {
    func remove() {
        
        guard let num = number else { return }
        guard let id = list?.giftCardId else { return }
        
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "PaymentAcceptController") as? PaymentAcceptController {
                
                loadVC.delegate = self
                loadVC.amountVal = price
                loadVC.isComeFrom = "MyFriend"
                loadVC.phone = num
                loadVC.id = id
                loadVC.msg = messageView.text
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
    
}
