//
//  MapController.swift
//  Gift3R
//
//  Created by Ranjit on 07/03/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit
import MapKit

class MapController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    let annotation = MKPointAnnotation()
    
    var urlString : String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let geocoder = CLGeocoder()
        
        if let  a = urlString {
            let address = a
            geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                if((error) != nil){
                    print("Error", error ?? "")
                }
                if let placemark = placemarks?.first {
                    let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                    print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                    
                    self.annotation.coordinate = CLLocationCoordinate2D(latitude: coordinates.latitude, longitude: coordinates.longitude)
                    self.annotation.title = a
                    self.mapView.addAnnotation(self.annotation)
                    self.mapView.showAnnotations(self.mapView.annotations, animated: true)
                }
            })
        }
        
    }
    

    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
