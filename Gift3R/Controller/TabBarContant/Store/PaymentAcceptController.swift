//
//  PaymentAcceptController.swift
//  Gift3R
//
//  Created by Ranjit on 19/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

protocol PaymentAcceptProtocal {
    func remove()
}

class PaymentAcceptController: UIViewController {
    
    var delegate:PaymentAcceptProtocal?
    
    @IBOutlet weak var firstname: UITextField!
    
    @IBOutlet weak var lastname: UITextField!
    
    @IBOutlet weak var cardNumber: MaxLengthTextField!
    
    @IBOutlet weak var visaBtn: UIButton!
    @IBOutlet weak var masterBtn: UIButton!
    @IBOutlet weak var discuverBtn: UIButton!
    @IBOutlet weak var amercanBtn: UIButton!
    
    @IBOutlet weak var amount: UITextField!
    @IBOutlet weak var month: UITextField!
    @IBOutlet weak var year: UITextField!
    @IBOutlet weak var cvv: MaxLengthTextField!
    
    var cardType  = "Visa"
    
    var amountVal  = ""

    var datasource: [String] = []
    let pickerView = UIPickerView()
    
    var fieldType: String = ""
    

    var monthArr: [String] = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    
    var yearArr: [String]  = []
    var yearArr1: [String]  = []
    
    var isComeFrom = ""
    var phone: String?
    var id: String?
    
    var msg: String?
    
    
    var alertPopUp = UIViewController()

    var thanksYouType: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        amount.text = "$\(amountVal).00"
        
        let pickerView1 = UIPickerView()
        pickerView1.delegate = self
        pickerView1.tag = 100

        month.inputView = pickerView1
        
        let pickerView2 = UIPickerView()
        pickerView2.delegate = self
        pickerView2.tag = 101

        year.inputView = pickerView2
        
        
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yy"
        let formattedDate = format.string(from: date)
        
        yearArr.append(formattedDate)
        
        if let id = Int(formattedDate) {
            var val : Int = id
            
            for _ in 0...10 {
                val = val + 1
                yearArr.append(String(val))
            }
            
        }

        yearFun()
        
    
    cardType = "Visa"
        
        
        if cardType == "American Express" {
            cvv.maxLength = 4
        } else {
            cvv.maxLength = 3
        }
        
    }
    
   
    func yearFun() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy"
        let formattedDate = format.string(from: date)
        
        yearArr1.append(formattedDate)
        
        if let id = Int(formattedDate) {
            var val : Int = id
            
            for _ in 0...10 {
                val = val + 1
                yearArr1.append(String(val))
            }
            
        }
    }
    

    @IBAction func infoBtn(_ sender: Any) {
        GlobalMethod.init().showAlert(title: "", message: "3-digit security code usually found on the back of your card. American Express Cards have a 4-digit code located on the front.", vc: self)
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        delegate?.remove()
    }
 
    @IBAction func visaBtnAction(_ sender: Any) {

        if visaBtn.imageView?.image == #imageLiteral(resourceName: "gray_radio") {
            visaBtn.setImage(#imageLiteral(resourceName: "red_radio"), for: .normal)
            masterBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            discuverBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            amercanBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            
            cardType = "Visa"
            
        }
        
        if cardType == "American Express" {
            cvv.maxLength = 4
        } else {
            cvv.maxLength = 3
        }
        
    }
    
    
    @IBAction func masterBtnAction(_ sender: Any) {
        
        if masterBtn.imageView?.image == #imageLiteral(resourceName: "gray_radio") {
            visaBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            masterBtn.setImage(#imageLiteral(resourceName: "red_radio"), for: .normal)
            discuverBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            amercanBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            
            cardType = "MasterCard"
        }
        
        if cardType == "American Express" {
            cvv.maxLength = 4
        } else {
            cvv.maxLength = 3
        }
        
        
    }
    
    @IBAction func discoverBtnAction(_ sender: Any) {
        if discuverBtn.imageView?.image == #imageLiteral(resourceName: "gray_radio") {
            visaBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            masterBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            discuverBtn.setImage(#imageLiteral(resourceName: "red_radio"), for: .normal)
            amercanBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            
            cardType = "Discover"

        }
        
        if cardType == "American Express" {
            cvv.maxLength = 4
        } else {
            cvv.maxLength = 3
        }
        
    }
    
    
    @IBAction func amercanBtnAction(_ sender: Any) {
        if amercanBtn.imageView?.image == #imageLiteral(resourceName: "gray_radio") {
            visaBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            masterBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            discuverBtn.setImage(#imageLiteral(resourceName: "gray_radio"), for: .normal)
            amercanBtn.setImage(#imageLiteral(resourceName: "red_radio"), for: .normal)
            
            cardType = "American Express"
        }
        
        
        if cardType == "American Express" {
            cvv.maxLength = 4
        } else {
            cvv.maxLength = 3
        }
        
        
    }
    
    
    @IBAction func payBtnAction(_ sender: Any) {
        
        guard let fname = firstname.text, fname != "" else {
            GlobalMethod.init().showAlert(title: "First Nmae", message: "Please enter the card holder first name!", vc: self)
            return
        }
        
        
        guard let lname = lastname.text, lname != "" else {
            GlobalMethod.init().showAlert(title: "Last Name", message: "Please enter the card holder last name!", vc: self)
            return
        }
        
        guard let cardNum = cardNumber.text, cardNum != "" else {
            GlobalMethod.init().showAlert(title: "Card Number", message: "Please enter card number!", vc: self)
            return
        }
        
      
        
        if cardNum.count == 16 {
            
            guard let mahinaVal = month.text, mahinaVal != "" else {
                GlobalMethod.init().showAlert(title: "Month", message: "Please enter the month!", vc: self)
                return
            }
            
            guard let saalVal = year.text, saalVal != "" else {
                GlobalMethod.init().showAlert(title: "Year", message: "Please enter the year!", vc: self)
                return
            }
            
            guard let ccv = cvv.text, ccv != "" else {
                GlobalMethod.init().showAlert(title: "CVV", message: "Please enter the CVV!", vc: self)
                return
            }
            
            if cardType == "American Express" {
                cvv.maxLength  = 4
                if ccv.count == 4 {
                    let username = fname + lname

                    paymentApi(username: username, cardNumber: cardNum, month: mahinaVal, year: saalVal, cvv: ccv)

                } else {
                    GlobalMethod.init().showAlert(title: "CVV", message: "Please enter valide cvv number!", vc: self)
                }

            } else {
                
                cvv.maxLength  = 3
                
                if ccv.count == 3 {
                    let username = fname + lname
                    
                    paymentApi(username: username, cardNumber: cardNum, month: mahinaVal, year: saalVal, cvv: ccv)
                    
                } else {
                    GlobalMethod.init().showAlert(title: "CVV", message: "Please enter valide cvv number!", vc: self)
                }
            }
            
        } else {
            
            GlobalMethod.init().showAlert(title: "Card not Valid", message: "Please enter the correct numbers!", vc: self)
        }
        
    }
    
}

extension PaymentAcceptController: NVActivityIndicatorViewable {
    
    func paymentApi(username: String, cardNumber: String, month : String, year: String, cvv: String) {
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }

        let parameters = [
            "session_id": sessionId,
            "card_number": cardNumber,
            "card_type":cardType,
            "expiry_date": month+year,
            "cvv":cvv,
            "amount":amountVal,
            "card_holdername":username
        ]
        
        print("Prams => \(parameters)")
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(payment, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["message"].stringValue == "Payment Processed successfully. Thanks you for your order" {
                    
                    if self.isComeFrom == "MyFriend" {
                        guard let num = self.phone, let i = self.id else {
                            return
                        }
                        self.buyByApi(number: num, id: i)
                        self.thanksYouType = "MyFridend"
                    } else {
                        guard let i = self.id else {
                            return
                        }
                        self.buyNow(id: i)
                        self.thanksYouType = "MySelf"
                    }
                    
                } else if JSON(res)["error"].string != "bad_request" {
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_msg"].stringValue, vc: self)
                    }
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_msg"].stringValue, vc: self)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
}

extension PaymentAcceptController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        if pickerView.tag == 100 {
            return monthArr.count
        }  else {
            return yearArr.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if pickerView.tag == 100 {
            return monthArr[row]
        } else {
            return yearArr1[row]
        }
        
    }
    
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            
            if pickerView.tag == 100 {
                month.text = monthArr[row]
            } else {
                year.text = yearArr[row]
            }
        }
    
}


extension PaymentAcceptController {
    
    func buyByApi(number: String, id: String) {
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        guard let message = msg else {
            return
        }
        
        let parameters = [
            "session_id": sessionId,
            "card_id": id,
            "phone_number": number,
            "message":message,
            "locale":""
            ]
        
        print("Prams => \(parameters)")
        
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(BuyCard, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["message"].string == "send cards succesfully" {
                    
                    DispatchQueue.main.async {
                        // GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["message"].string ?? "Please try again after few moments!", vc: self)
                        
//                        self.delegate?.remove()
                        
                        self.removeThankYou()
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}

extension PaymentAcceptController: ThankYouprotocal {
    
    func removeThankYou() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "ThankYouController") as? ThankYouController {
                loadVC.delegate = self

                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            
            if self.thanksYouType == "MyFridend" {
                alertPopUp.view.removeFromSuperview()
            } else {
                alertPopUp.view.removeFromSuperview()
            }
            alertPopUp = UIViewController()
        }
    }
    
}

extension PaymentAcceptController {
    
    func buyNow(id: String) {
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        let parameters = [
            "session_id": sessionId,
            "card_id": id,
            "phone_number": "",
            "locale":""
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(BuyCard, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("\(JSON(res))")
                
                if JSON(res)["message"].string == "buy cards succesfully" {
                    
                    DispatchQueue.main.async {
                        self.removeThankYou()
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
}

