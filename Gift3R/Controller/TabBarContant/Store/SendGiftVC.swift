//
//  SendGiftVC.swift
//  Gift3R
//
//  Created by Ranjit on 19/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

protocol SendFiftProtocal {
    func dissmisView()
    func mySelfAction()
    func aFriendAction()
}

import ContactsUI

class SendGiftVC: UIViewController, BuyCardMessageProtocal, CNContactPickerDelegate {
    
    var delegate:SendFiftProtocal?
    
    var alertPopUp =  UIViewController()

    var amount = ""
    
    var info: StoresGift?
    var shopInfo:Stores?
    
    let contactPicker = CNContactPickerViewController()

    
    var isComeFrom: String?
    var phoneFromContactList: String?
    

    @IBOutlet weak var myselftView: DesignableView!
    @IBOutlet weak var aFriend: DesignableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactPicker.delegate = self
        
        if isComeFrom == "OtherUserFav" {
            myselftView.isHidden = true
            aFriend.isHidden = true
        } else {
            myselftView.isHidden = false
            aFriend.isHidden = false
        }
        
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        delegate?.dissmisView()
    }
    
    @IBAction func asendToFriend(_ sender: UIButton) {
        self.present(contactPicker, animated: true, completion: nil)
    }
    
    @IBAction func mySelfAction(_ sender: UIButton) {
        remove()
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        var username: String = ""
        
        for i in contact.givenName {
            username.append(i)
        }
        
        
        for number in contact.phoneNumbers {
            
            let phoneNumber = number.value
            
            if username == "" {
                username = phoneNumber.stringValue
            }
            
            DispatchQueue.main.async {
                let dialogMessage = UIAlertController(title: "Alert", message: "You are about to purchase this card and send to \(username)?", preferredStyle: .alert)
                
                // Create OK button with action handler
                let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    
                    if !(self.alertPopUp.isViewLoaded) {
                        let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
                        if let loadVC = storyboard.instantiateViewController(withIdentifier: "BuyCardMessageController") as? BuyCardMessageController {
                            loadVC.delegate = self
                            
                            loadVC.number = phoneNumber.stringValue
                            loadVC.list = self.info
                            loadVC.shop = self.shopInfo
                            
                            loadVC.price = self.amount
                            
                            self.addChild(loadVC)
                            self.view.addSubview(loadVC.view)
                            self.alertPopUp.view.alpha = 0
                            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                                self.alertPopUp.view.alpha = 1
                            }, completion: nil)
                            
                            self.alertPopUp = loadVC
                        }
                    }
                
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
            }
            
        }
        
    }
    
}

extension SendGiftVC: PaymentAcceptProtocal {
    func remove() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "PaymentAcceptController") as? PaymentAcceptController {
                loadVC.delegate = self
                loadVC.amountVal = amount
                loadVC.id = info?.giftCardId
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }

}


extension SendGiftVC: SendGiftMessageProtocal {
    
    func removeVC() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "SendGiftMessageController") as? SendGiftMessageController {
                loadVC.delegate = self
//                loadVC.info = dataForSend
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
    
}
