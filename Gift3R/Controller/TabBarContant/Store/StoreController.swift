//
//  StoreController.swift
//  Gift3R
//
//  Created by Ranjit on 15/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class StoreController: UIViewController {
    
    @IBOutlet weak var storesCollectionView: UICollectionView!

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var emptyMsgLbl: UILabel?
    
    var storeList: [Stores] = []
    var searchedStoreList: [Stores] = []
    var searchActive : Bool = false

    var char: String?
    var pageNum: Int = 1
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh(_:)), for: UIControl.Event.valueChanged)
        
        refreshControl.tintColor = #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1)
        
        return refreshControl
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.storesCollectionView?.addSubview(self.refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        searchActive = false
        getStoresList(page: "\(pageNum)")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension StoreController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/2 - 25, height: 208)
    }
}

extension StoreController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if searchActive {
            if searchedStoreList.count == 0 {
                emptyMsgLbl?.isHidden = false
            } else {
                emptyMsgLbl?.isHidden = true
            }
            return searchedStoreList.count
        } else {
            if storeList.count == 0 {
                emptyMsgLbl?.isHidden = false
            } else {
                emptyMsgLbl?.isHidden = true
            }
            return storeList.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell_Id", for: indexPath) as! StoreCell
        
        if searchActive {
            cell.setUpCell(info: searchedStoreList[indexPath.row])
        } else {
            cell.setUpCell(info: storeList[indexPath.row])
        }
        
        return cell
    }
    
}

extension StoreController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if searchActive {
            let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailController
            vc.storeListAndDetails = searchedStoreList[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            
            let storyBoard = UIStoryboard.init(name: "TabBarContant", bundle: nil)
            let vc = storyBoard.instantiateViewController(withIdentifier: "StoreDetail") as! StoreDetailController
            vc.storeListAndDetails = storeList[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
    }
    
}

extension StoreController: NVActivityIndicatorViewable {
    
    func getStoresList(page: String) -> Void {
        
        startAnimating(CGSize(width: 60, height: 60), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.8900398612, green: 0.238706708, blue: 0.194413811, alpha: 1))
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": "",
            "limit": "20",
            "page": page
        ]
        
        print("Prams => \(parameters)")

        Alamofire.request(StoreListing, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async { self.stopAnimating() }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].stringValue == "Session Expired"{
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
                if JSON(res)["error_description"].string == nil {
                    self.storeList.removeAll()
                    for i in JSON(res).arrayValue{
                        self.storeList.append(Stores.init(info: i))
                    }
                    self.pageNum = 1
                    DispatchQueue.main.async(execute: {
                        self.storesCollectionView.reloadData()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }

            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}


/* Search by city */
extension StoreController : UISearchDisplayDelegate, UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
        storesCollectionView?.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
        searchBar.text = nil
        
        // Remove focus from the search bar.
        searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = true;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        char = searchText
        searchByApi(char: searchText)
        searchActive = true;
    }
    
}


extension StoreController {

    func searchByApi(char: String)  {
        
//        startAnimating(CGSize(width: 60, height: 60), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.8900398612, green: 0.238706708, blue: 0.194413811, alpha: 1))
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": char,
            "limit": "20",
            "page": "1"
        ]
        
        print("Prams => \(parameters)")
        
        Alamofire.request(StoreListing, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
//                    DispatchQueue.main.async { self.stopAnimating() }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                
                DispatchQueue.main.async(execute: {
//                    self.stopAnimating()
                })
                
                if JSON(res)["error_description"].string == nil {
                    
                    self.searchedStoreList = []
                    
                    for i in JSON(res).arrayValue{
                        self.searchedStoreList.append(Stores.init(info: i))
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.storesCollectionView.reloadData()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    //GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
//                    self.stopAnimating()
                })
            }
        }
    }
    
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        let sessionId = UserDefaults.standard.value(forKey: "SessionId") ?? ""
        
        let parameters = [
            "session_id": "\(sessionId)",
            "search": "",
            "limit": "20",
            "page": "1"
        ]
        
        print("Prams => \(parameters)")
        
        Alamofire.request(StoreListing, method:.get, parameters:parameters, encoding: URLEncoding.default).responseJSON { response in
            
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async {
                        self.refreshControl.endRefreshing()
                    }
                    return
                }
                
                print("Responce => \(JSON(res))")
                
                if JSON(res)["error_description"].stringValue == "Session Expired"{
                    UserDefaults.standard.removeObject(forKey: "SessionId")
                    UserDefaults.standard.removeObject(forKey: "Email")
                    UserDefaults.standard.removeObject(forKey: "PhoneNimber")
                    
                    UserDefaults.standard.removeObject(forKey: "Name")
                    UserDefaults.standard.removeObject(forKey: "UserId")
                    
                    DispatchQueue.main.async(execute: {
                        let vc = (self.navigationController?.viewControllers[0])
                        self.navigationController?.popToViewController(vc!, animated: true)
                    })
                }
                
                DispatchQueue.main.async(execute: {
                    self.refreshControl.endRefreshing()
                })
                
                if JSON(res)["error_description"].string == nil {
                    self.storeList.removeAll()
                    for i in JSON(res).arrayValue{
                        self.storeList.append(Stores.init(info: i))
                    }
                    self.pageNum = 1
                    DispatchQueue.main.async(execute: {
                        self.storesCollectionView.reloadData()
                    })
                    
                } else {
                    
                    DispatchQueue.main.async {
                        GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["error_description"].string ?? "Please try again after few moments!", vc: self)
                    }
                    
                }
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                   // GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.refreshControl.endRefreshing()
                })
            }
        }
    }
    
    
}
