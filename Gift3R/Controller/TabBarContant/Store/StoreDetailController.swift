//
//  StoreDetailController.swift
//  Gift3R
//
//  Created by Ranjit on 17/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

import ContactsUI

import Alamofire
import SwiftyJSON
import NVActivityIndicatorView

class StoreDetailController: UIViewController, CNContactPickerDelegate {
    
    @IBOutlet weak var avaliableGiftstableView: UITableView!
    
    @IBOutlet weak var headerTxt: UILabel!
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var tableViewHeightConst: NSLayoutConstraint!
    
    @IBOutlet weak var giftBtn: UIButton!
    
    fileprivate let imageLoader = ImageLoader.sharedInstance
    fileprivate var index : Int?
    
    var storeListAndDetails: Stores?
    
    let contactPicker = CNContactPickerViewController()
    var cardId: String?
    
    var selectedIndex: Int?
    
    var alertPopUp =  UIViewController()
    
    var totalAmount = ""
    
    var saingleItemInfo: StoresGift?
    
    var isComeFrom: String?
    var phoneContactList: String?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupInfo()
        
        contactPicker.delegate = self

        print(storeListAndDetails?.gifts.count ?? "")
        
        self.avaliableGiftstableView.reloadData()
        
        self.avaliableGiftstableView.isScrollEnabled = false
        
        var height: CGFloat = 0
        
        for cell in self.avaliableGiftstableView.visibleCells {
            height += cell.bounds.height
        }
        
        DispatchQueue.main.async {
            self.tableViewHeightConst.constant = height
            self.avaliableGiftstableView.layoutIfNeeded()
        }
        
    }
    
    func setupInfo() {
        headerTxt.text = storeListAndDetails?.name
        //        website.text = storeListAndDetails?.website
        //        address.text = storeListAndDetails?.address
        //        number.text = storeListAndDetails?.phoneNo
        loadImage(defaultImage : #imageLiteral(resourceName: "LoadingImage"), url : storeListAndDetails?.image, imageView: img)
        
        if storeListAndDetails?.status == "1" {
            self.giftBtn.setImage(UIImage(named: "giftred2"), for: .normal)
        } else {
            self.giftBtn.setImage(UIImage(named: "giftgray2"), for: .normal)
        }
        
    }
    
    fileprivate func loadImage(defaultImage : UIImage?, url : String?, imageView : UIImageView?){
        imageView?.image = defaultImage
        imageLoader.loadImage(url , token: { () -> (Int) in
            return (self.index ?? 0)
        }) { (success, image) in
            if(!success){
                return
            }
            imageView?.image = image
        }
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func locationAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MapController") as! MapController
        guard let url =  storeListAndDetails?.address else { return }
        vc.urlString = url
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func webAction(_ sender: Any) {
        guard let url =  storeListAndDetails?.website else {
            return
        }
        
        print(url)
        
        if let url = NSURL(string: url) {
            UIApplication.shared.openURL(url as URL)
        } else {
            GlobalMethod.init().showAlert(title: "Alert", message:"Website url is not avaliable!", vc: self)
        }
    }
    
    @IBAction func callAction(_ sender: Any) {
        if let num  = storeListAndDetails?.phoneNo {
            guard let number = URL(string: "tel://" + num) else { return }
            UIApplication.shared.open(number)
            //UIApplication.shared.open(number, options: [:], completionHandler: nil)
        } else {
            GlobalMethod.init().showAlert(title: "Alert", message:"Mobile number is not avaliable!", vc: self)
        }
    
    }
    
    @IBAction func giftAction(_ sender: Any) {
        
        if let num = storeListAndDetails?.phoneNo, let id = storeListAndDetails?.storeId {
            addToFavByApi(number: num, id: id)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        for number in contact.phoneNumbers {
            
            let phoneNumber = number.value
            
            DispatchQueue.main.async {
                let dialogMessage = UIAlertController(title: "Alert", message: "Are you sure you want to send this card?", preferredStyle: .alert)
                
                // Create OK button with action handler
                let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    guard let id = self.cardId else { return }
                    self.sendbyApi(number: phoneNumber.stringValue, id: id)
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                    
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
            }
            
        }
        
    }

}

extension StoreDetailController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storeListAndDetails?.gifts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvaliableGitsfCell_id") as! AvaliableGitsfCell
        cell.delegate = self
        if let index = storeListAndDetails?.gifts[indexPath.row] {
            cell.setUpInfo(info: index)
        }
        
        return cell
    }
    
}

extension StoreDetailController: AvaliableGitsProtocal, SendGiftMessageProtocal {
    
    func remove() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
            self.alertPopUp.view.alpha = 0
        }, completion: nil)
        alertPopUp.view.removeFromSuperview()
        alertPopUp = UIViewController()
    }
    
    
    func messageBtn(cell: AvaliableGitsfCell) {

        guard let indexPath = self.avaliableGiftstableView.indexPath(for: cell)
            else{
                return
        }

        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "SendGiftMessageController") as? SendGiftMessageController {
                loadVC.delegate = self
                
                if let index = storeListAndDetails?.gifts[indexPath.row] {
                    loadVC.msg = index.message
                } else {
                    loadVC.msg = "No Message"
                }
                
                loadVC.redOnly = "StoreDetails"
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        }

    }
    
    
    func info(cell: AvaliableGitsfCell) {
        GlobalMethod().showAlert(title: "", message: "? GIFT3Rapp Electronic Gift Cards, cary a balance and can be used on multiple visits till the balance is zero. Your eGift Cards never expire. eGift Cards are not re-loadable.", vc: self)
    }
    
    
    func buy(cell: AvaliableGitsfCell) {
        
        guard let indexPath = self.avaliableGiftstableView.indexPath(for: cell)
            else {
            return
        }
        
        selectedIndex = indexPath.row
        
        saingleItemInfo = storeListAndDetails?.gifts[indexPath.row]
        
        
        
        if isComeFrom == "OtherUserFav" {
            removeVC()
        } else {
            dissmisView()
        }
        
    }
    
    func send(cell: AvaliableGitsfCell) {
        
        guard let indexPath = self.avaliableGiftstableView.indexPath(for: cell)
            else {
            return
        }
        
        print(indexPath.row)
        
        selectedIndex = indexPath.row
        
        saingleItemInfo = storeListAndDetails?.gifts[indexPath.row]

        if let index = storeListAndDetails?.gifts[indexPath.row] {
            totalAmount = index.price ?? ""
        }
        
        if isComeFrom == "OtherUserFav" {
            removeVC()
        } else {
            dissmisView()
        }
        
    }
    
}

extension StoreDetailController: NVActivityIndicatorViewable {
    
        func sendbyApi(number: String, id: String) {
            
            guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
                return
            }
            
            let parameters = [
                "session_id": sessionId,
                "card_id": id,
                "phone_number": number
            ]
            
            print("Prams => \(parameters)")
            /*
             UserDefaults.standard.value(forKey: "Device_Token")
             UserDefaults.standard.value(forKey: "FCM_Token")
             */
            startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
            
            Alamofire.request(SendMyCards, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
                switch response.result {
                case .success:
                    guard  let res = response.result.value else {
                        DispatchQueue.main.async(execute: {
                            self.stopAnimating()
                        })
                        return
                    }
                    
                    print("Responce => \(JSON(res))")
                    
                    if JSON(res)["error_description"].string != nil {
                        
                        DispatchQueue.main.async {
                            GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["message"].string ?? "Please try again after few moments!", vc: self)
                        }
                        
                        
                    } else {
                        
                        DispatchQueue.main.async {
                            GlobalMethod.init().showAlert(title: "Alert", message: JSON(res)["message"].string ?? "Please try again after few moments!", vc: self)
                        }
                        
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    
                case .failure(let error):
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                        self.stopAnimating()
                    })
                }
            }
        }
    
}


extension StoreDetailController: SendFiftProtocal {
    
    func dissmisView() {
    
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "SendGiftVC") as? SendGiftVC {
                loadVC.delegate = self
                loadVC.shopInfo = storeListAndDetails
                loadVC.info = saingleItemInfo
                loadVC.amount = totalAmount
                
                if isComeFrom == "OtherUserFav" {
                    loadVC.phoneFromContactList = phoneContactList
                    loadVC.isComeFrom = "OtherUserFav"
                }
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
    func mySelfAction() {
        if let index = storeListAndDetails?.gifts[selectedIndex!] {
            if let cardId = index.giftCardId, let number = storeListAndDetails?.phoneNo {
                
                let dialogMessage = UIAlertController(title: "Alert", message: "Are you sure you want to buy this card?", preferredStyle: .alert)
                
                // Create OK button with action handler
                let ok = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
                    // self.buyNow(number: number, id: cardId)
                })
                
                // Create Cancel button with action handlder
                let cancel = UIAlertAction(title: "No", style: .cancel) { (action) -> Void in
                    
                }
                
                //Add OK and Cancel button to dialog message
                dialogMessage.addAction(ok)
                dialogMessage.addAction(cancel)
                
                // Present dialog message to user
                self.present(dialogMessage, animated: true, completion: nil)
                
                
            }
        }
    }
    
    func aFriendAction() {
        if let index = storeListAndDetails?.gifts[selectedIndex!] {
            cardId = index.giftCardId
            self.present(contactPicker, animated: true, completion: nil)
        }
    }
    
}

extension StoreDetailController {
    // addTofav
    
    func addToFavByApi(number: String, id: String) {
        
        guard let sessionId = UserDefaults.standard.value(forKey: "SessionId") else {
            return
        }
        
        let parameters = [
            "session_id": sessionId,
            "store_id": id,
        ]
        
        print("Prams => \(parameters)")
        
        /*
         UserDefaults.standard.value(forKey: "Device_Token")
         UserDefaults.standard.value(forKey: "FCM_Token")
         */
        startAnimating(CGSize(width: 65, height: 65), type: NVActivityIndicatorType(rawValue: 2)!, color: #colorLiteral(red: 0.954166472, green: 0.239402473, blue: 0.2319205105, alpha: 1))
        
        Alamofire.request(addTofav, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                guard  let res = response.result.value else {
                    DispatchQueue.main.async(execute: {
                        self.stopAnimating()
                    })
                    return
                }
                
                print("\(JSON(res))")
                
                if JSON(res)["status"].intValue == 1 {
                    
                    DispatchQueue.main.async {
                        self.giftBtn.setImage(UIImage(named: "giftred2"), for: .normal)
                    }
                    
                } else if JSON(res)["status"].intValue == 0 {
                    
                    DispatchQueue.main.async {
                        
                        if self.isComeFrom == "Registry" {
                            self.navigationController?.popViewController(animated: true)
                        }
                        
                        self.giftBtn.setImage(UIImage(named: "giftgray2"), for: .normal)
                    }
                    
                }
                
                DispatchQueue.main.async(execute: {
                    self.stopAnimating()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: "Alert", message: error.localizedDescription, vc: self)
                    self.stopAnimating()
                })
            }
        }
    }
    
}


extension StoreDetailController : BuyCardMessageProtocal{
    
    func removeVC() {
        if !(alertPopUp.isViewLoaded) {
            let storyboard = UIStoryboard(name: "TabBarContant", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "BuyCardMessageController") as? BuyCardMessageController {
                loadVC.delegate = self
                
                loadVC.number = phoneContactList
                loadVC.list = self.saingleItemInfo
                loadVC.shop = self.storeListAndDetails
                loadVC.price = self.totalAmount
                
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                self.alertPopUp.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.alertPopUp.view.alpha = 1
                }, completion: nil)
                
                self.alertPopUp = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.alertPopUp.view.alpha = 0
            }, completion: nil)
            alertPopUp.view.removeFromSuperview()
            alertPopUp = UIViewController()
        }
    }
    
    
}
