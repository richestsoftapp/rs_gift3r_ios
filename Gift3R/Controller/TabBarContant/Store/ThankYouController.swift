
//
//  ThankYouController.swift
//  Gift3R
//
//  Created by Ranjit on 27/02/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import UIKit

protocol ThankYouprotocal {
    func removeThankYou()
}

class ThankYouController: UIViewController {

    var delegate:ThankYouprotocal?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func goBack(_ sender: UIButton) {
        let story = UIStoryboard(name: "TabBarContant", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "TabBar") as! TabBarController
        vc.selectedIndex = 1
        self.navigationController?.pushViewController(vc, animated: true)
        delegate?.removeThankYou()
    }
    
}
