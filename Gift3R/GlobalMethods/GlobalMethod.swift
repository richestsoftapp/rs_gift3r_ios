//
//  GlobalMethod.swift
//  Art_B
//
//  Created by Ranjit on 24/05/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var isValidEmail: Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
}

class GlobalMethod: NSObject {
   
    func isLoggedIn() -> Bool {
        
        let info = UserDefaults.standard.value(forKey: "")
        
        if info != nil {
            return true
        }
        
        return false
    }
    
    /*
     Image's Border Radius
     */
    func imgCircle( image: UIImageView, borderWidth: Int, masksToBounds: Bool, borderColor: UIColor, clipsToBounds: Bool) -> Void {
        image.layer.borderWidth = CGFloat(borderWidth)
        image.layer.masksToBounds = masksToBounds
        image.layer.borderColor = borderColor.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    func viewCircle( image: UIView, borderWidth: Int, masksToBounds: Bool, borderColor: UIColor, clipsToBounds: Bool) -> Void {
        image.layer.borderWidth = CGFloat(borderWidth)
        image.layer.masksToBounds = masksToBounds
        image.layer.borderColor = borderColor.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    func showAlert(title: String, message: String, vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {vc.present(alert, animated: true) }
    }
    
    func UTCToLocal(date:String) -> String {
        var dateStr:String = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let converteddate = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if converteddate != nil {
            dateStr = dateFormatter.string(from: converteddate!)
        }
        return dateStr
    }
    
}
