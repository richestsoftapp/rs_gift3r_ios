
//
//  Gifts.swift
//  Gift3R
//
//  Created by Ranjit on 24/10/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

/*
 
 
 
 {
 "gift_card_id": "22",
 "available_balance": 3,
 "gift_card_name": "$5",
 "store_name": "Campus Flowers",
 "store_image": "http:\/\/demo.richestsoft.in\/GIFT3R\/public\/thumb\/uploads\/M7YnAW9Y_ScreenShot2018-09-04at6.38.29PM.png",
 "mypurchasedcard_id": "6",
 "message": null
 },
 
 

*/

import Foundation
import SwiftyJSON


class Gifts: NSObject {
    
    var giftCardId: String?
    var availableBalance: String?
    var giftCardName: String?
    
    var storeName: String?
    var storeImage: String?
    
    var myPurchasedcardId: String?
    var message: String?
    
    override init(){
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        giftCardId = jsonInfo["gift_card_id"]?.stringValue
        availableBalance = jsonInfo["available_balance"]?.stringValue
        giftCardName = jsonInfo["gift_card_name"]?.stringValue
        storeName = jsonInfo["store_name"]?.stringValue
        storeImage = jsonInfo["store_image"]?.stringValue
        
        myPurchasedcardId = jsonInfo["mypurchasedcard_id"]?.stringValue
        message = jsonInfo["message"]?.stringValue
        
    }
    
}
