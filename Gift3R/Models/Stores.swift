
//
//  Stores.swift
//  Gift3R
//
//  Created by Ranjit on 24/10/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

/*

 "store_id": "2",
 "name": "Gurpreet",
 "image": "http://demo.richestsoft.com/GIFT3R/GIFT3R/public/thumb/uploads/XspMODnS_watermelon.jpg",
 "zipcode": "456856",
 "address": "hsp",
 "city": "hsp",
 "website": "zara.com",
 "phone_no": "875685",
 "email": "zara@gmail.com",
 "available_cards": [
 {
 "gift_card_id": "2",
 "gift_card_name": "test",
 "price": "5"
 },
 {
 "gift_card_id": "12",
 "gift_card_name": "test",
 "price": "75"
 },
 {
 "gift_card_id": "13",
 "gift_card_name": "test123",
 "price": "100"
 }
 ]

*/

import Foundation
import SwiftyJSON

class Stores: NSObject {
    
    var storeId: String?
    var name: String?
    var image: String?
    var zipcode: String?
    var address: String?
    var city: String?
    var website: String?
    var phoneNo: String?
    var email: String?
    
    var status: String?
    
    
    var gifts: [StoresGift] = []
    
    
    override init(){
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        storeId = jsonInfo["store_id"]?.stringValue
        name = jsonInfo["name"]?.stringValue
        image = jsonInfo["image"]?.stringValue
        zipcode = jsonInfo["zipcode"]?.stringValue
        address = jsonInfo["address"]?.stringValue
        city = jsonInfo["city"]?.stringValue
        website = jsonInfo["website"]?.stringValue
        phoneNo = jsonInfo["phone_no"]?.stringValue
        email = jsonInfo["email"]?.stringValue
        status = jsonInfo["status"]?.stringValue
        
        for i in jsonInfo["available_cards"]?.arrayValue ?? [] {
            gifts.append(StoresGift.init(info: i))
        }
        
    }
    
}

/*
 
 {
 "gift_card_id" : 26,
 "message" : null,
 "price" : 100,
 "gift_card_name" : "Ranjit card"
 }
 
 
 */

class StoresGift: NSObject {
    
    var giftCardId: String?
    var giftCardName: String?
    var message : String?
    var price: String?
    
    override init(){
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        message = jsonInfo["message"]?.stringValue
        giftCardId = jsonInfo["gift_card_id"]?.stringValue
        giftCardName = jsonInfo["gift_card_name"]?.stringValue
        price = jsonInfo["price"]?.stringValue

    }
    
    
}
